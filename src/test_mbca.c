/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file main.c
 * @brief test main
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * test programs main
 */

#include "mbca_assistant.h"

#include <gtk/gtk.h>
#include <glib.h>

#include <getopt.h>
#include <locale.h>
#include <string.h>

static void
dump_configuration (MBCAConfiguration* conf)
{
	g_print ("name:      %s\n", conf->name);
	switch (conf->type)
	{
		case MBCA_DEVICE_BLUETOOTH:
		{
			g_print ("type:      %s\n", "bluetooth");
			break;
		}
		case MBCA_DEVICE_SERIAL:
		{
			g_print ("type:      %s\n", "serial");
			break;
		}
		case MBCA_DEVICE_HAL:
		{
			g_print ("type:      %s\n", "hal");
			break;
		}
		case MBCA_DEVICE_PSEUDO:
		{
			g_print ("type:      %s\n", "pseudo");
			break;
		}
		default:
		{
			g_return_if_reached ();
		}
	}
	g_print ("device:    %s\n", conf->device);
	g_print ("baud:      %s\n", conf->baud);
	
	/* just take the first one */
	g_print ("provider:  %s\n", ((MBCAServiceProviderName*)(conf->provider->names->data))->name);
	
	switch (conf->provider->type)
	{
		case MBCA_NETWORK_GSM:
		{
			g_print ("network:   %s\n", "GSM");
			g_print ("apn:       %s\n", conf->provider->gsm.apn);
			break;
		}
		case MBCA_NETWORK_CDMA:
		{
			g_print ("network:   %s\n", "CDMA");
			break;
		}
		default:
		{
			g_return_if_reached ();
		}
	}
	
	if (conf->provider->username)
	{
		g_print ("username:  %s\n", conf->provider->username);
	}
	if (conf->provider->password)
	{
		g_print ("password:  %s\n", conf->provider->password);
	}
	if (conf->provider->dns1)
	{
		g_print ("dns1:      %s\n", conf->provider->dns1);
	}
	if (conf->provider->dns2)
	{
		g_print ("dns2:      %s\n", conf->provider->dns1);
	}
	if (conf->provider->gateway)
	{
		g_print ("gateway:   %s\n", conf->provider->gateway);
	}
}

void
run_gui_state_changed (MBCAAssistant* assistant,
				   MBCAAssistantState state,
				   gpointer user_data)
{
	GSList** assistants = user_data;
	
	switch (state)
	{
		case MBCA_STATE_READY:
		case MBCA_STATE_RUNNING: 
		{
			return;
		}
		case MBCA_STATE_DONE:
		{
			MBCAConfiguration* conf;
			conf = mbca_assistant_get_configuration (assistant);
			dump_configuration (conf);
			mbca_free_configuration (conf);
			
			/* FALLTHROUGH */
		}
		case MBCA_STATE_ABORTED:
		{	
			*assistants = g_slist_remove_all (*assistants, assistant);
			g_object_unref (assistant);
		}
	}	
}

void
full_button_clicked_cb (GtkWidget* button, gpointer user_data)
{
	GSList** assistants = user_data;
	MBCAAssistant* assistant;
	gint ret;

	assistant = mbca_assistant_new ();
	g_signal_connect (G_OBJECT (assistant), "state-changed",
				   G_CALLBACK (run_gui_state_changed), assistants);
	
	*assistants = g_slist_append (*assistants, assistant);
	ret = mbca_assistant_run (assistant);
}

void
preset_button_clicked_cb (GtkWidget* button, gpointer user_data)
{
	GSList** assistants = user_data;
	MBCAAssistant* assistant;
	gint ret;

	assistant = mbca_assistant_new ();
	g_signal_connect (G_OBJECT (assistant), "state-changed",
				   G_CALLBACK (run_gui_state_changed), assistants);
	
	*assistants = g_slist_append (*assistants, assistant);
	ret = mbca_assistant_run_for_device (assistant, MBCA_DEVICE_SERIAL,
								  "/dev/ttyS0", "9600");
}

void
pseudo_button_clicked_cb (GtkWidget* button, gpointer user_data)
{
	GSList** assistants = user_data;
	MBCAAssistant* assistant;
	gint ret;

	assistant = mbca_assistant_new ();
	g_signal_connect (G_OBJECT (assistant), "state-changed",
				   G_CALLBACK (run_gui_state_changed), assistants);
	
	*assistants = g_slist_append (*assistants, assistant);
	ret = mbca_assistant_run_for_device (assistant, MBCA_DEVICE_PSEUDO,
								  NULL, NULL);
}

void
abort_button_clicked_cb (GtkWidget* button, gpointer user_data)
{
	GSList** assistants = user_data;
	GSList* iter;

	iter = *assistants;
	while (iter) {
		MBCAAssistant* assistant = iter->data;
		
		/* state_changed_cb will remove the assistant from the list so we
		 * let's take the next assistant before that */
		iter = iter->next; 
			
		mbca_assistant_abort (assistant);
	}
}

static GtkWindow*
create_gui ()
{
	GtkWindow* testwindow;
	GtkAlignment* align;
	GtkWidget* vbox;

	GtkWidget* full_button;
	GtkWidget* preset_button;
	GtkWidget* pseudo_button;
	GtkWidget* abort_button;
	GtkWidget* quit_button;
	
	GSList* assistants = NULL;
	
	testwindow = GTK_WINDOW (gtk_window_new (GTK_WINDOW_TOPLEVEL));
	gtk_widget_set_size_request (GTK_WIDGET (testwindow), 250, -1);
	gtk_window_set_title (testwindow, "Test libmbca");

	align = GTK_ALIGNMENT (gtk_alignment_new (0.5, 0.5, 1.0, 0.0));
	gtk_alignment_set_padding (align, 12, 12, 12, 12);
	gtk_container_add (GTK_CONTAINER (testwindow), GTK_WIDGET (align));

	
	vbox = GTK_WIDGET (gtk_vbox_new (FALSE, 6));
	gtk_container_add (GTK_CONTAINER (align), vbox);

	full_button = gtk_button_new_with_label ("Full");
	gtk_box_pack_start (GTK_BOX(vbox), full_button, FALSE, FALSE, 0);

	preset_button = gtk_button_new_with_label ("Preset");
	gtk_box_pack_start (GTK_BOX(vbox), preset_button, FALSE, FALSE, 0);

	pseudo_button = gtk_button_new_with_label ("Pseudo");
	gtk_box_pack_start (GTK_BOX(vbox), pseudo_button, FALSE, FALSE, 0);
	
	gtk_box_pack_start (GTK_BOX(vbox), gtk_vseparator_new(), FALSE, FALSE, 6);
	
	abort_button = gtk_button_new_with_label ("Abort");
	gtk_box_pack_start (GTK_BOX(vbox), abort_button, FALSE, FALSE, 0);
	
	gtk_box_pack_start (GTK_BOX(vbox), gtk_vseparator_new(), FALSE, FALSE, 6);	
	
	quit_button = gtk_button_new_with_label ("Quit");
	gtk_box_pack_start (GTK_BOX(vbox), quit_button, FALSE, FALSE, 0);
	
	g_signal_connect (G_OBJECT (full_button), "clicked",
				   G_CALLBACK (full_button_clicked_cb), &assistants);
	g_signal_connect (G_OBJECT (preset_button), "clicked",
				   G_CALLBACK (preset_button_clicked_cb), &assistants);
	g_signal_connect (G_OBJECT (pseudo_button), "clicked",
				   G_CALLBACK (pseudo_button_clicked_cb), &assistants);
	g_signal_connect (G_OBJECT (abort_button), "clicked",
				   G_CALLBACK (abort_button_clicked_cb), &assistants);	
	g_signal_connect (G_OBJECT (quit_button), "clicked",
				   G_CALLBACK (gtk_main_quit), NULL);	
	
	gtk_widget_show_all (GTK_WIDGET (testwindow));

	return testwindow;
}


static void
run_gui ()
{	
	GtkWindow* window;
	
	window = create_gui ();
	
	g_signal_connect (G_OBJECT (window), "destroy",
				   G_CALLBACK (gtk_main_quit), NULL);
	
	gtk_main ();
}

void
run_once_state_changed (MBCAAssistant* assistant,
				    MBCAAssistantState state,
				    gpointer user_data)
{
	switch (state)
	{
		case MBCA_STATE_READY:
		case MBCA_STATE_RUNNING: 
		{
			return;
		}
		case MBCA_STATE_DONE:
		{
			MBCAConfiguration* conf;
			conf = mbca_assistant_get_configuration (assistant);
			dump_configuration (conf);
			mbca_free_configuration (conf);
			
			/* FALLTHROUGH */
		}
		case MBCA_STATE_ABORTED:
		{	
			gtk_main_quit();
		}
	}
}
	
static void
run_once (MBCADeviceType type, gchar** device, gchar** nicename)
{
	MBCAAssistant* assistant;
	gint ret;
	
	assistant = mbca_assistant_new ();
	
	if (type != MBCA_DEVICE_NONE)
	{
		if (!device)
		{
			switch (type)
				{
				case MBCA_DEVICE_BLUETOOTH:
				{
					*device = g_strdup ("00:34:37:FA:15:01");
					break;
				}
				case MBCA_DEVICE_HAL:
				{
					*device = g_strdup ("/org/freedesktop/Hal/devices/usb_d"
									"evice_421_410_noserial_if8_serial_"
									"unknown_1");
					break;
				}
				case MBCA_DEVICE_SERIAL:
				{
					*device = g_strdup ("/dev/ttyS1");
					if (!*nicename)
					{
						*nicename = g_strdup ("9600");
					}
					break;
				}
				default:
				{
					g_return_if_reached ();
				}
			}
		}
		
		if (!*nicename)
		{
			*nicename = g_strdup ("little cute device");
		}
		
		ret = mbca_assistant_run_for_device (assistant,
									  type,
									  *device,
									  *nicename);
	}
	else
	{
		ret = mbca_assistant_run (assistant);
	}	
	
	g_signal_connect (G_OBJECT (assistant), "state-changed",
				   (GCallback)run_once_state_changed,
				   NULL);
	/* entering gtk_main */
	gtk_main ();

	g_object_unref (assistant);
}

static void
print_usage(char* prog)
{	
	g_print("Usage:\n");
	g_print("\t%s [options]\n", "test_mbca");
	g_print("\n");
	g_print("options may be:\n");
	g_print("\t-h, --help      ---  print this help\n");
	g_print("\n");
	g_print("\t    --bluetooth ---  preset bluetooth device\n");
	g_print("\t    --hal       ---  preset HAL device\n");
	g_print("\t    --serial    ---  preset serial device\n");
	g_print("\t                     preset baudrate with 'nice'\n");
	g_print("\t    --device    ---  type specific device address\n");
	g_print("\t    --nicename  ---  nice name for the device\n");
	g_print("\n");
	g_print("\t    --gui       ---  show GUI with cute buttons\n");	
	g_print("\n");
}


int
main (int argc, char* argv[])
{
	int c = 0;
	int option_index = 0;
	
	MBCADeviceType type = MBCA_DEVICE_NONE;
	gchar* device = NULL;
	gchar* nicename = NULL;
		
	gint i;
	gint ret;
	
	gboolean use_gui = FALSE;
	
	struct option long_options[] = 
	{
		{"help"		, no_argument		, 0  , 'h'},
		{"bluetooth"   , no_argument		, 0  , 'b'},
		{"hal"		, no_argument		, 0  , 'u'}, /* 'u' for historical
											    * reasons :)
											    */
		{"serial"		, no_argument		, 0  , 's'},
		{"device"		, required_argument , 0  , 'd'},
		{"nicename"	, required_argument , 0  , 'n'},
		{"gui"		, no_argument		, 0  , 'g'},
		{0 , 0, 0, 0}
	};

	gtk_init (&argc, &argv);	
	g_thread_init (NULL);	
	
	while ((c = getopt_long(argc, argv,
					    "hbusdng",
					    long_options,
					    &option_index)) != -1)
	{
		switch (c)
		{
			case 'b':
			{
				/* bluetooth*/
				type = MBCA_DEVICE_BLUETOOTH;
				break;
			}
			case 'd':
			{
				/* --device */
				device = g_strdup (optarg);
				break;
			}
			case 'g':
			{
				use_gui = TRUE;
				break;
			}
			case 'h':
			{
				/* --help, -h */
				print_usage(argv[0]);
				return 0;
			}
			case 'n':
			{
				nicename = g_strdup (optarg);
				break;
			}

			case 's':
			{
				/* --serial */
				type = MBCA_DEVICE_SERIAL;
				break;
			}
			case 'u':
			{
				/* --hal */
				type = MBCA_DEVICE_HAL;
				break;
			}
				
			default:
			{
				print_usage(argv[0]);
				return -1;
			}
		}

	}

	setlocale (LC_ALL, "");
	textdomain ("test_mbca");
	
	if (use_gui)
	{
		run_gui ();
	}
	else
	{
		run_once (type, &device, &nicename);
	}
	
	g_free (device);
	g_free (nicename);
	
	return 0;
}



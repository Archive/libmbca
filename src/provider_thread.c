/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file provider_thread.c
 * @brief provider page resource loading
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * Shared provider page resources are loaded in separate thread. This file
 * contains resource loading functionality.
 */
 
#include "provider_thread.h"
#include "gnome-panel/system-timezone.h"


#ifdef LIBGWEATHER_PRIOR_2_23_6
/**
 * ret must be freed
 */
gchar*
mbca_alpha2name (const gchar* code,
			  GtkTreeModel* gweather_database,
			  volatile gboolean* abort)
{
/* from gweather-xml.c:
 *=============================================================================
 * <gweather format="1.0">
 * <region>
 *  <name>Name of the region</name>
 *  <name xml:lang="xx">Translated Name</name>
 *  <name xml:lang="zz">Another Translated Name</name>
 *  <country>
 *   <name>Name of the country</name>
 *   <iso-code>2-letter ISO 3166 code for the country</iso-code>
 *   <tz-hint>default timezone</tz-hint>
 *   <location>
 *    <name>Name of the location</name>
 *    <code>IWIN code</code>
 *    <zone>Forecast code (North America, Australia, UK only)</zone>
 *    <radar>Weather.com radar map code (North America only)</radar>
 *    <coordinates>Latitude and longitude as DD-MM[-SS][H] pair</coordinates>
 *   </location>
 *   <state>
 *     <location>
 *       ....
 *     </location>
 *     <city>
 *      <name>Name of city with multiple locations</city>
 *      <zone>Forecast code</zone>
 *      <radar>Radar Map code</radar>
 *      <location>
 *        ...
 *      </location>
 *     </city>
 *   </state>
 *  </country>
 * </region>
 * <gweather>
 * ============================================================================
 * The thing to note is that each country can either contain different locations
 * or be split into "states" which in turn contain a list of locations.
 *
 * <iso-code> can appear at the country or location level. <tz-hint>
 * can appear at country, state, or location.
 *
 * treemodel is constructed as following:
 *
 * region1
 *   |_____country1
 *           |______location1
 *           |______location2
 *   |_____country2
 *           |______location3
 *           |______location4
 * region2
 *   |_____country3
 *           |______state1
 *           |        |______location5
 *           |        |______location6
 *           |______state2 
 *                    |______location5
 *                    |______location6
 *
 *
 * ISO codes are stored in locations, but every location of a country has the
 * same code. Thus it's sufficient to check the first location of a country or
 * first location of first state if a country.
 */
	
	GtkTreeIter region;
		
	gtk_tree_model_get_iter_first (gweather_database, &region);
	do
	{	
		GtkTreeIter country;
		if ( !gtk_tree_model_iter_children (gweather_database,
									 &country,
									 &region))
		{
			/* no countries */
			continue;
		}
		
		do
		{
			GtkTreeIter location;
			WeatherLocation* loc;

			if (!gtk_tree_model_iter_children (gweather_database,
										&location,
										&country))
			{
				/* no locations */
				continue;
			}
			
			gtk_tree_model_get (gweather_database, &location,
							GWEATHER_XML_COL_POINTER, &loc,
							-1);
			if (!loc)
			{
				/* country has states, location means state */
				GtkTreeIter statelocation;
				if (!gtk_tree_model_iter_children (gweather_database,
											&statelocation, 
											&location))
				{
					/* no state locations */
					continue;
				}
				gtk_tree_model_get (gweather_database, &statelocation,
								GWEATHER_XML_COL_POINTER, &loc,
								-1);
				if(!loc)
				{
					/* skip */
					continue;
				}
			}
			
			if (g_ascii_strcasecmp (code, loc->country_code) == 0)
			{
				gchar* name;
				gtk_tree_model_get (gweather_database, &country,
								GWEATHER_XML_COL_LOC, &name,
								-1);				
				return name;
			}
		}
		while (gtk_tree_model_iter_next (gweather_database,
								   &country) && !(*abort));
	}
	while (gtk_tree_model_iter_next (gweather_database, &region) && !(*abort));

	return NULL;
}
#else 
/* libgweather 2.23.6
==================
 ...
Locations.xml
 ...
	 - every <location> node is inside a <city> node with a real city name
 ...
*/

/**
 * ret must be freed
 */
gchar*
mbca_alpha2name (const gchar* code,
			  GtkTreeModel* gweather_database,
			  volatile gboolean* abort)
{
	GtkTreeIter region;
		
	gtk_tree_model_get_iter_first (gweather_database, &region);
	do
	{	
		GtkTreeIter country;
		if ( !gtk_tree_model_iter_children (gweather_database,
									 &country,
									 &region))
		{
			/* no countries */
			continue;
		}
		
		do
		{
			GtkTreeIter city;
		
			if (!gtk_tree_model_iter_children (gweather_database,
										&city,
										&country))
			{
				/* no cities */
				continue;
			}			
			
			do
			{
				GtkTreeIter location;
				WeatherLocation* loc;

				if (!gtk_tree_model_iter_children (gweather_database,
											&location,
											&city))
				{
					/* no locations */
					continue;
				}
			
				gtk_tree_model_get (gweather_database, &location,
								GWEATHER_XML_COL_POINTER, &loc,
								-1);
				if (!loc)
				{
					/* country has states, location means state */
					GtkTreeIter statelocation;
					if (!gtk_tree_model_iter_children (gweather_database,
												&statelocation, 
												&location))
					{
						/* no state locations */
						continue;
					}
					gtk_tree_model_get (gweather_database, &statelocation,
									GWEATHER_XML_COL_POINTER, &loc,
									-1);
					if(!loc)
					{
						/* skip */
						continue;
					}
				}
			
				if (g_ascii_strcasecmp (code, loc->country_code) == 0)
				{
					gchar* name;
					gtk_tree_model_get (gweather_database, &country,
									GWEATHER_XML_COL_LOC, &name,
									-1);				
					return name;
				}
			} while (gtk_tree_model_iter_next (gweather_database,
										&city) && !(*abort));
		}
		while (gtk_tree_model_iter_next (gweather_database,
								   &country) && !(*abort));
	}
	while (gtk_tree_model_iter_next (gweather_database, &region) && !(*abort));

	return NULL;
}
#endif /* LIBGWEATHER_PRIOR_2_23_6 */

/**
 * @brief populate provider list store
 *
 * Populates service provider list store from database.
 *
 * @param data pointer to ServiceProvider
 * @param user_data pointer to GtkListStore
 */
static void
mbca_provider_foreach (gpointer data, gpointer user_data)
{
	GtkListStore* provider_store = user_data;
	MBCAServiceProvider* provider = data;
	const gchar* const* locales;
	GtkTreeIter iter;
	const gchar* name;;
		
	/* choose the correct localized name for the provider */		
	locales = g_get_language_names ();
	g_return_if_fail (provider->names);
	GSList* nameiter = provider->names;
	gint best_match = G_MAXINT;
	name = NULL;
	gint i;
	while (nameiter)
	{
		MBCAServiceProviderName* namestruct = nameiter->data;

		/* shamelessly adapted from libgweather gweather-xml.c */
		for( i = 0; namestruct->lang && locales[i] && i < best_match; i++ )
		{
			if( !g_ascii_strcasecmp( locales[i], namestruct->lang ) )
			{
				name = namestruct->name;
				best_match = i;
				break;
			}
		}
		nameiter = nameiter->next;
	}
	if (!name)
	{
		/* use the first name as default */
		name = ((MBCAServiceProviderName*)provider->names->data)->name;
	}
	
	gtk_list_store_append (provider_store, &iter);
	gtk_list_store_set (provider_store, &iter,
					PROVIDER_NAME_COLUMN, name,
					PROVIDER_DATA_COLUMN, provider,
					-1);
}

/**
 * @brief populate country list store
 *
 * Populates country GtkListStore from database.
 *
 * @param data pointer to ServiceProviderCountry
 * @param user_data pointer to mbcaAppData
 */
static void
mbca_country_foreach (gpointer data, gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	ServiceProviderCountry* country = data;
	GtkListStore* provider_store;
	GtkTreeIter iter;
	gchar* name;

	if (priv->abort_provider_thread)
	{
		return;
	}
	
	name = mbca_alpha2name (country->code,
					    priv->gweather_database,
					    &priv->abort_provider_thread);
	if (!name)
	{
		/* code missing from libgweather X( */
		g_warning ("Country name for code \"%s\" is missing from libgweather",
				 country->code);
		name = g_strdup (_("missing from libgweather")); /* dup so it can be
												* freed with g_free
												*/
	}
	
	provider_store = gtk_list_store_new (PROVIDER_N_COLUMNS,
								  G_TYPE_STRING,   /* name */
								  G_TYPE_POINTER); /* ServiceProvider */
	g_object_ref_sink (provider_store);
	g_slist_foreach (country->providers, mbca_provider_foreach, provider_store);
	
	gtk_list_store_append (priv->country_store, &iter);
	gtk_list_store_set (priv->country_store, &iter,
					COUNTRY_NAME_COLUMN, name,
					COUNTRY_CODE_COLUMN, country->code,
					COUNTRY_PROVIDERS_COLUMN, provider_store,
					COUNTRY_SORTED_COLUMN, FALSE,
					-1);
	g_free (name);
}

#ifdef LIBGWEATHER_PRIOR_2_23_6
/**
 * code must not be freed
 */
static const gchar*
mbca_match_timezone(const gchar* timezone, 
				GtkTreeModel* gweather_database, 
				GtkTreeIter country,
				volatile gboolean* abort)
{
	GtkTreeIter location;
	WeatherLocation* loc;

	if (!gtk_tree_model_iter_children (gweather_database,
								&location,
								&country))
	{
		/* no locations */
		return NULL;
	}
	
	do
	{
		gtk_tree_model_get (gweather_database, &location,
						GWEATHER_XML_COL_POINTER, &loc,
						-1);
		if (!loc)
		{			
			/* country has states , location means state */
			GtkTreeIter statelocation;
			if (!gtk_tree_model_iter_children (gweather_database,
										&statelocation, 
										&location))
			{
				/* no state locations */
				continue;
			}
			
			
			do
			{
				gtk_tree_model_get (gweather_database, &statelocation,
								GWEATHER_XML_COL_POINTER, &loc,
								-1);
				if(!loc)
				{
					/* skip */
					continue;
				}
			}
			while (gtk_tree_model_iter_next (gweather_database, &statelocation)
				  && !(*abort));
			
			if (!loc)
			{
				continue;
			}
		}
		if (loc->tz_hint)
		{
			if (g_ascii_strcasecmp (timezone, loc->tz_hint) == 0)
			{		
				return loc->country_code;
			}
		}
	}
	while (gtk_tree_model_iter_next (gweather_database, &location)
		  && !(*abort));
	
	return NULL;
}
#else
/**
 * code must not be freed
 */
static const gchar*
mbca_match_timezone(const gchar* timezone, 
				GtkTreeModel* gweather_database, 
				GtkTreeIter country,
				volatile gboolean* abort)
{
	GtkTreeIter city;
	GtkTreeIter location;
	WeatherLocation* loc;

	if (!gtk_tree_model_iter_children (gweather_database,
								&city,
								&country))
	{
		/* no cities */
		return NULL;
	}
	
	do 
	{
	
		if (!gtk_tree_model_iter_children (gweather_database,
									&location,
									&city))
		{
			/* no locations */
			return NULL;
		}
	
		do
		{
			gtk_tree_model_get (gweather_database, &location,
							GWEATHER_XML_COL_POINTER, &loc,
							-1);
			if (!loc)
			{			
				/* country has states , location means state */
				GtkTreeIter statelocation;
				if (!gtk_tree_model_iter_children (gweather_database,
											&statelocation, 
											&location))
				{
					/* no state locations */
					continue;
				}
			
			
				do
				{
					gtk_tree_model_get (gweather_database, &statelocation,
									GWEATHER_XML_COL_POINTER, &loc,
									-1);
					if(!loc)
					{
						/* skip */
						continue;
					}
				}
				while (gtk_tree_model_iter_next (gweather_database, &statelocation)
					  && !(*abort));
			
				if (!loc)
				{
					continue;
				}
			}
			if (loc->tz_hint)
			{
				if (g_ascii_strcasecmp (timezone, loc->tz_hint) == 0)
				{		
					return loc->country_code;
				}
			}
		}
		while (gtk_tree_model_iter_next (gweather_database, &location)
			  && !(*abort));
	} while (gtk_tree_model_iter_next (gweather_database, &city)
		    && !(*abort));
	
	return NULL;
}
#endif /* LIBGWEATHER_PRIOR_2_23_6 */

/**
 * code must not be freed!
 */
static const gchar*
mbca_get_system_country (GtkTreeModel* gweather_database, 
					volatile gboolean* abort)
{
	gchar* timezone;
	GtkTreeIter region;
	const gchar* code = NULL;

	timezone = system_timezone_find ();
	
	gtk_tree_model_get_iter_first (gweather_database, &region);
	do
	{	
		GtkTreeIter country;
		if ( !gtk_tree_model_iter_children (gweather_database,
									 &country,
									 &region))
		{
			/* no countries */
			continue;
		}
		
		do
		{
			code = mbca_match_timezone (timezone,
								   gweather_database,
								   country,
								   abort);
			if (code)
			{
				g_free (timezone);
				return code;
			}

		}
		while (gtk_tree_model_iter_next (gweather_database,
								   &country)
			  && !(*abort));
	}
	while (gtk_tree_model_iter_next (gweather_database, &region) && !(*abort));

	if (!code)
	{
		/* tz-hint missing from libgweather X( */
		g_warning ("tz-hint \"%s\" is missing from libgweather",
				 timezone);
	}
	
	g_free (timezone);
	return NULL;
}


gpointer
mbca_load_provider_page_resources_thread_func (gpointer data)
{
	MBCAAssistantPrivate* priv = data;
	const gchar* code;
	gint ret;

	/**
	 * @bug main program should be aborted if something bad happens here
	 *      but the user should be informed, because the UI is most propably
	 *      already visible.
	 */
	
	g_debug ("loading provider resources..");
	
	if (priv->abort_provider_thread)
	{
		return NULL;
	}
	
	ret = parse_service_provider_xml (MOBILE_BROADBAND_PROVIDER_INFO,
							    &(priv->database));
	if (priv->abort_provider_thread)
	{
		return NULL;
	}
	g_return_val_if_fail (!ret, NULL);

	priv->gweather_database = gweather_xml_load_locations ();
	g_return_val_if_fail (priv->gweather_database, NULL);
	g_object_ref_sink (priv->gweather_database);
	if (priv->abort_provider_thread)
	{
		return NULL;
	}

	g_slist_foreach (priv->database, mbca_country_foreach, priv);
	if (priv->abort_provider_thread)
	{
		return NULL;
	}
	
	/* select correct country */
	code = mbca_get_system_country (priv->gweather_database,
							  &priv->abort_provider_thread);
	priv->country_code = g_strdup (code);
	
	
	g_debug ("provider resources loaded");
	

	/** @todo fixme */
#if 0
	GSList* iter;
	
	/* time for some attaching */
	g_debug ("attaching resources to waiting instances");
	g_mutex_lock (priv->instances_mutex);
	for (iter = priv->waiting_instances;
		iter && !priv->abort_provider_thread;
		iter = iter->next)
	{
		MBCAAssistantPrivate* ipriv = iter->data;
		mbca_provider_attach_resources (priv, ipriv);
	}
	g_slist_free (priv->waiting_instances);
	g_debug ("resources attached");
	priv->provider_thread_ready = TRUE;
	g_mutex_unlock (priv->instances_mutex);
#endif
	priv->provider_thread_ready = TRUE;
	g_mutex_unlock (priv->trigger_mutex);
	
	return NULL;
}
 
void
mbca_provider_attach_resources (MBCAAssistantPrivate* priv)
{
	GtkTreeViewColumn* column;
	GtkWidget* country_list;
	GtkExpander* expander;
	GtkWidget* page;
	gboolean found;
	
	g_debug ("attaching resources");
	
	country_list = GTK_WIDGET (gtk_builder_get_object (priv->builder,
											 "country_treeview"));
	g_return_if_fail (country_list);	
	
	gtk_tree_view_set_model (GTK_TREE_VIEW (country_list),
						GTK_TREE_MODEL (priv->country_store));
	
	/* sort the country list */
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (country_list), 
								COUNTRY_NAME_COLUMN);
	gtk_tree_view_column_set_sort_order (column, 
								  GTK_SORT_ASCENDING);
	gtk_tree_view_column_clicked (column);		
		
	found = FALSE;
	if (priv->country_code)
	{
		/* set country list to correct row */
		GtkTreeIter iter;
		gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->country_store),
								 &iter);
		do
		{
			gchar* modelcode;
						
			gtk_tree_model_get (GTK_TREE_MODEL (priv->country_store), &iter,
							COUNTRY_CODE_COLUMN, &modelcode,
							-1);
			if (g_ascii_strcasecmp (priv->country_code, modelcode) == 0)
			{
				found = TRUE;
				g_free (modelcode);
				break;
			}
			g_free (modelcode);
		}
		while (gtk_tree_model_iter_next (GTK_TREE_MODEL (priv->country_store),
								   &iter));
		if (found)
		{
			GtkTreeSelection* selection;
			GtkTreePath* path;
			GtkTreeView* view = GTK_TREE_VIEW (country_list);
			selection = gtk_tree_view_get_selection (view);
			
			path = gtk_tree_model_get_path (GTK_TREE_MODEL (priv->country_store),
									  &iter);
			gtk_tree_selection_select_path (selection,
									  path);
			gtk_tree_view_scroll_to_cell (view, path, NULL, TRUE, 0.5, 0.5);
			gtk_tree_path_free (path);
		}
		/* else continue to expand code.. */
	}
	if (!found){
		/* expand country list */
		expander = GTK_EXPANDER (gtk_builder_get_object (priv->builder,
											    "country_expander"));
		g_return_if_fail (expander);
		gtk_expander_set_expanded (expander, TRUE);
	}
	
	page = gtk_assistant_get_nth_page (priv->assistant, PAGE_PROVIDER);
	gtk_widget_set_sensitive (page, TRUE);
}

/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file mbca_assistant.c
 * @brief Mobile Broadband Configuration Assistant
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * Main file of Mobile Broadband Configuration Assistant. Contains assistant
 * callbacks and initialization functions.
 */
 
#include "provider_thread.h"
#include "mbca_assistant.h"
#include "callbacks.h"

#include "mbca_type_builtins.h"

#include <stdlib.h>

static GObjectClass *parent_class = NULL;


/**
 * @brief connect signals
 *
 * Helper macro for connect_signals().
 *
 * @param name name of the widget
 * @param signal name of the signal
 * @param callback function
 *
 * @note requires symbol <i>priv</i> that is pointer to MBCAAssistantPriv
 */
#define CONNECT(name, signal, callback)\
	object = gtk_builder_get_object (priv->builder, name);  \
	if (!object) \
	{ \
		g_error ("no widget \'%s\' in UI definition", name); \
	} \
	g_signal_connect (object, signal, G_CALLBACK (callback), priv)

/**
 * @brief connect selection signals
 *
 * Helper macato for connect_signals().
 *
 * @param name of the GtkTreeView
 * @param signal name of the signal
 * @param callback funtion
 *
 * @note requires symbols <i>selection</i>, <i>object</i> and <i>priv</i>
 */
#define CONNECT_SELECTION(name, signal, callback)\
	object = gtk_builder_get_object (priv->builder, name);  \
	if (!object) \
	{ \
		g_error ("no widget \'%s\' in UI definition", name); \
	} \
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (object)); \
	g_signal_connect (G_OBJECT (selection), signal, G_CALLBACK (callback), priv)

/**
 * @brief connect GUI signals
 *
 * Connect widget signals to callbacks.
 *
 * @param assistant assistant to connect signals
 */
static void
connect_signals (MBCAAssistant* assistant)
{
	MBCAAssistantPrivate* priv = assistant->priv;
	GtkTreeSelection* selection;	
	GObject* object;

	/* assistant */
	object = gtk_builder_get_object (priv->builder, "mbca_assistant"); 	
	if (!object) 
	{ 
		g_error ("no widget \'%s\' in UI definition", "mbca_assistant"); \
	}
	g_signal_connect (object, "apply", 
				   G_CALLBACK (mbca_assistant_apply_cb), assistant);
	g_signal_connect (object, "cancel", 
				   G_CALLBACK (mbca_assistant_cancel_cb), assistant);
	g_signal_connect (object, "close", 
				   G_CALLBACK (mbca_assistant_close_cb), assistant);
	g_signal_connect (object, "prepare", 
				   G_CALLBACK (mbca_assistant_prepare_cb), assistant);
	
	/* connection method page */
	CONNECT ("bluetooth_radiobutton", "clicked",
		    mbca_bluetooth_radiobutton_clicked_cb);
	CONNECT ("builtin_radiobutton", "clicked",
		    mbca_builtin_radiobutton_clicked_cb);
	CONNECT ("pcmcia_radiobutton", "clicked",
		    mbca_pcmcia_radiobutton_clicked_cb);
	CONNECT ("serialcable_radiobutton", "clicked",
		    mbca_serialcable_radiobutton_clicked_cb);
	CONNECT ("usb_radiobutton", "clicked",
		    mbca_usb_radiobutton_clicked_cb);
	
	/* Bluetooth page */
	CONNECT ("bluetooth_manual_checkbutton", "toggled",
		    mbca_bluetooth_manual_checkbutton_toggled_cb);
	CONNECT ("bluetooth_manual_entry", "changed",
		    mbca_bluetooth_manual_entry_changed_cb);
	/* btselector signal is connected in setup_bluetooth_page() */
	
	/* serial cable page */
	CONNECT ("serial_manual_checkbutton", "toggled",
		   mbca_serial_manual_checkbutton_toggled_cb);
	CONNECT ("serial_manual_entry", "changed",
		    mbca_serial_manual_entry_changed_cb);
	CONNECT ("serial_manual_baud_entry", "changed",
		    mbca_serial_manual_baud_entry_changed_cb);
	CONNECT_SELECTION ("serial_treeview", "changed",
				    mbca_serial_treeview_selection_changed_cb);
	CONNECT_SELECTION ("serial_baud_treeview", "changed",
				    mbca_serial_baud_treeview_selection_changed_cb);
	
	/* HAL page */
	CONNECT_SELECTION ("hal_treeview", "changed",
				    mbca_hal_treeview_selection_changed_cb);
	
	/* service provider page */
	CONNECT ("country_treeview", "query-tooltip",
		    mbca_country_code_qyery_tooltip_cb);
	CONNECT_SELECTION ("country_treeview", "changed",
				    mbca_country_selection_changed_cb);
	CONNECT_SELECTION ("provider_treeview", "changed",
				    mbca_provider_selection_changed_cb);
	
	/* summary page */
	CONNECT ("summary_name_entry", "changed",
		    mbca_summary_name_entry_changed_cb);
}

/**
 * @brief determine next page
 *
 * from GtkAssistant documentation:
 * "A function used by gtk_assistant_set_forward_page_func() to know which is
 *  the next page given a current one. It's called both for computing the next
 *  page when the user presses the 'forward' button and for handling the
 *  behavior of the 'last' button."
 *
 * @param current_page The page number used to calculate the next page
 * @param data pointer to mbcaAppData
 */
static gint
mbca_page_func (gint current_page,
			 gpointer data)
{
	MBCAAssistantPrivate* priv = data;
	
	switch (current_page)
	{
		case PAGE_INTRO:
		{
			if (priv->device_is_preset)
			{
				return PAGE_PROVIDER;
			}
			return PAGE_METHOD;
		}
		case PAGE_METHOD:
		{			
			/* next page is chosen based on radio buttons on method_page */
			GtkToggleButton* button;
			GObject* gobject;
			
			gobject = gtk_builder_get_object (priv->builder,
									    "bluetooth_radiobutton");
			g_return_val_if_fail (gobject, 0);
			button = GTK_TOGGLE_BUTTON (gobject);
			if (gtk_toggle_button_get_active (button))
			{
				return PAGE_BLUETOOTH;
			}

			gobject = gtk_builder_get_object (priv->builder,
									    "serialcable_radiobutton");
			g_return_val_if_fail (gobject, 0);
			button = GTK_TOGGLE_BUTTON (gobject);
			if (gtk_toggle_button_get_active (button))
			{
				return PAGE_SERIAL;
			}
			
			gobject = gtk_builder_get_object (priv->builder,
									    "builtin_radiobutton");
			g_return_val_if_fail (gobject, 0);
			button = GTK_TOGGLE_BUTTON (gobject);
			if (gtk_toggle_button_get_active (button))
			{
				return PAGE_HAL;
			}
			gobject = gtk_builder_get_object (priv->builder,
									    "pcmcia_radiobutton");
			g_return_val_if_fail (gobject, 0);
			button = GTK_TOGGLE_BUTTON (gobject);
			if (gtk_toggle_button_get_active (button))
			{
				return PAGE_HAL;
			}
			gobject = gtk_builder_get_object (priv->builder,
									    "usb_radiobutton");
			g_return_val_if_fail (gobject, 0);
			button = GTK_TOGGLE_BUTTON (gobject);
			if (gtk_toggle_button_get_active (button))
			{
				return PAGE_HAL;
			}
			
			return PAGE_METHOD;
		}
		case PAGE_BLUETOOTH:
		case PAGE_SERIAL:
		case PAGE_HAL:
		{
			return PAGE_PROVIDER;
		}
		default:
		{
			return current_page + 1;
		}
	}	 
}

/**
 * Sets up country and provider lists from database and resolves the default
 * country or expands the country expander.
 *
 * @param app private application data
 *
 * @returns ' 0' on success
 * @returns '-1' on error
 */
static gint
setup_provider_page (MBCAAssistantPrivate* priv)
{
	GtkListStore* provider_store;
	GtkTreeSelection* selection;	
	GtkCellRenderer* renderer;
	GtkTreeViewColumn* column;
	GtkWidget* provider_list;	
	GtkWidget* country_list;

	country_list = GTK_WIDGET (gtk_builder_get_object (priv->builder,
											 "country_treeview"));
	provider_list = GTK_WIDGET (gtk_builder_get_object (priv->builder,
											 "provider_treeview"));
	g_return_val_if_fail (country_list, -1);
	g_return_val_if_fail (provider_list, -1);	
	
	/* country list columns */	
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("",
											 renderer,
											 "text", COUNTRY_CODE_COLUMN,
											 NULL);
	gtk_tree_view_column_set_sort_column_id (column, COUNTRY_CODE_COLUMN);
	gtk_tree_view_column_set_sort_order (column, GTK_SORT_ASCENDING);
	gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (country_list), column);
	gtk_widget_set_has_tooltip (country_list, TRUE);
	
	column = gtk_tree_view_column_new_with_attributes (_("Country"),
											 renderer,
											 "text", COUNTRY_NAME_COLUMN,
											 NULL);
	gtk_tree_view_column_set_sort_column_id (column, COUNTRY_NAME_COLUMN);
	gtk_tree_view_column_set_sort_order (column, GTK_SORT_ASCENDING);
	gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (country_list), column);
	gtk_tree_view_set_search_column (GTK_TREE_VIEW (country_list), COUNTRY_NAME_COLUMN);

	
	/* service provider list columns */
	column = gtk_tree_view_column_new_with_attributes (_("Provider"),
											 renderer,
											 "text", PROVIDER_NAME_COLUMN,
											 NULL);
	gtk_tree_view_column_set_sort_column_id (column, PROVIDER_NAME_COLUMN);
	gtk_tree_view_column_set_sort_order (column, GTK_SORT_ASCENDING);
	gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (provider_list), column);
	gtk_tree_view_set_search_column (GTK_TREE_VIEW (provider_list), PROVIDER_NAME_COLUMN);

	
	/* set selection mode */
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (country_list));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (provider_list));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);	

	
	/* set empty model */
	provider_store = gtk_list_store_new (PROVIDER_N_COLUMNS,
								  G_TYPE_STRING,		/* name */
								  G_TYPE_POINTER);		/* data */
	gtk_tree_view_set_model (GTK_TREE_VIEW (provider_list),
						GTK_TREE_MODEL (provider_store));
			
	return 0;
}

/**
 * @brief set up and initialize HAL page
 *
 * Creates treemodels and sets columns for treeviews.
 *
 * @param private application data
 * @param hal_store shared list of HAL devices
 *
 * @returns ' 0' on success
 * @returns '-1' on error
 */
static gint
setup_hal_page (MBCAAssistantPrivate* priv, GtkListStore* hal_store)
{
	GtkTreeSelection* selection;
	GtkCellRenderer* renderer;
	GtkTreeViewColumn* column;
	GObject* hal_treeview;
	
	hal_treeview = gtk_builder_get_object (priv->builder, "hal_treeview");
	g_return_val_if_fail (hal_treeview, -1);

	renderer = gtk_cell_renderer_text_new ();
		
	column = gtk_tree_view_column_new_with_attributes (_("Product"),
											 renderer,
											 "text", HAL_PRODUCT_COLUMN,
											 NULL);
	gtk_tree_view_column_set_sort_column_id (column, HAL_PRODUCT_COLUMN);
	gtk_tree_view_column_set_sort_order (column, GTK_SORT_ASCENDING);
	gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (hal_treeview), column);

	column = gtk_tree_view_column_new_with_attributes (_("Vendor"),
											 renderer,
											 "text", HAL_VENDOR_COLUMN,
											 NULL);
	gtk_tree_view_column_set_sort_column_id (column, HAL_VENDOR_COLUMN);
	gtk_tree_view_column_set_sort_order (column, GTK_SORT_ASCENDING);
	gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW (hal_treeview), column);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (hal_treeview));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);	
	
	gtk_tree_view_set_model (GTK_TREE_VIEW (hal_treeview),
						GTK_TREE_MODEL (hal_store));	
	return 0;
}

/**
 * @brief set up and initialize serial page
 *
 * Creates treemodels and sets columns for treeviews.
 *
 * @param priv private application data
 * @param serial_store shared list of serial devices
 * @param baud_store shared list of baud rates
 *
 * @returns ' 0' on success
 * @returns '-1' on error
 */
static gint
setup_serial_page (MBCAAssistantPrivate* priv,
			    GtkListStore* serial_store,
			    GtkListStore* baud_store)
{
	GtkTreeSelection* selection;
	GtkCellRenderer* renderer;
	GtkTreeViewColumn* column;
	GtkTreeView* serial_view;
	GtkTreeView* baud_view;
	
	serial_view = GTK_TREE_VIEW (gtk_builder_get_object (priv->builder,
											   "serial_treeview"));
	baud_view = GTK_TREE_VIEW (gtk_builder_get_object (priv->builder,
											 "serial_baud_treeview"));
	g_return_val_if_fail (serial_view, -1);
	g_return_val_if_fail (baud_view, -1);

	renderer = gtk_cell_renderer_text_new ();
	
	column = gtk_tree_view_column_new_with_attributes ("",
											 renderer,
											 "text", SERIAL_SUBSYSTEM_COLUMN,
											 NULL);
	gtk_tree_view_column_set_sort_column_id (column, SERIAL_SUBSYSTEM_COLUMN);
	gtk_tree_view_column_set_sort_order (column, GTK_SORT_ASCENDING);
	gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_view_append_column (serial_view, column);
	
	column = gtk_tree_view_column_new_with_attributes (_("Port"),
											 renderer,
											 "text", SERIAL_PORT_COLUMN,
											 NULL);
	gtk_tree_view_column_set_sort_column_id (column, SERIAL_PORT_COLUMN);
	gtk_tree_view_column_set_sort_order (column, GTK_SORT_ASCENDING);
	gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_view_append_column (serial_view, column);

	
	column = gtk_tree_view_column_new_with_attributes (_("Baud Rate"),
											 renderer,
											 "text", SERIALBAUD_RATE_COLUMN,
											 NULL);
	gtk_tree_view_column_set_sort_column_id (column, SERIALBAUD_RATE_COLUMN);
	gtk_tree_view_column_set_sort_order (column, GTK_SORT_ASCENDING);
	gtk_tree_view_column_set_sort_indicator (column, TRUE);
	gtk_tree_view_append_column (baud_view, column);

	/* set selection */
	selection = gtk_tree_view_get_selection (serial_view);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);
	selection = gtk_tree_view_get_selection (baud_view);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);
	
	gtk_tree_view_set_model (baud_view, GTK_TREE_MODEL (baud_store));
	gtk_tree_view_set_model (serial_view, GTK_TREE_MODEL (serial_store));
	
	return 0;
}

static void
setup_bluetooth_page (MBCAAssistantPrivate* priv)
{
	GObject* placeholder;

	placeholder = gtk_builder_get_object (priv->builder, 
								   "bluetooth_placeholder_vbox");
	g_return_if_fail (placeholder);
	
	priv->btselector = bluetooth_device_selection_new(NULL);
	g_object_set (priv->btselector,
			    "show-search", FALSE,
			    "show-bonding", FALSE,
			    "show-device-type", FALSE,
			    "show-device-category", FALSE,
			    "device-type-filter", BLUETOOTH_TYPE_PHONE,
			    NULL);

	priv->btrescan_thread = g_thread_create (mbca_bluetooth_rescan_cb,
									priv,
									TRUE,
									NULL);
	g_return_if_fail (priv->btrescan_thread);
	
	gtk_widget_show (priv->btselector);		

	gtk_box_pack_start (GTK_BOX (placeholder), priv->btselector, 
					TRUE, TRUE, 0);
	
	g_signal_connect(priv->btselector, "selected-device-changed",
				  G_CALLBACK(mbca_bluetooth_selected_device_changed), priv);
}

/** 
 * Sets all the connection method radiobuttons on method_page inactive and
 * thus force the user to choose one.
 *
 * @param app private application data
 */
static void
set_method_radio_buttons_inactive(MBCAAssistantPrivate* priv)
{
	GObject* togglebutton;	

	/* it seems that gtk does not allow every radiobutton in a group to be
	 * inactive. Let's solve this problem with a hidden button.
	 */
	togglebutton = gtk_builder_get_object (priv->builder,
								    "method_none_radiobutton");
	g_return_if_fail (togglebutton);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (togglebutton),
							TRUE);
}

/******************************************************************************
 * HAL *
 *******/
static void
find_hal_info (LibHalContext *ctx,
			const char* udi,
			gchar** vendor,
			gchar** product)
{
	char* parent;
	
	*vendor = NULL;
	*product = NULL;
	
	parent = libhal_device_get_property_string (ctx, udi, "info.parent", NULL);
	while (parent)
	{
		char* tmp;
		char* subsystem = libhal_device_get_property_string (ctx,
												   parent,
												   "info.subsystem",
												   NULL);		
		/** @todo pcmcia, pccard, expresscard */ 
		if (g_utf8_collate (subsystem, "usb_device") == 0)
		{
			char* halproduct;
			char* halvendor;
			
			halproduct = libhal_device_get_property_string (ctx,
												   parent,
												   "usb_device.product",
												   NULL);
			halvendor = libhal_device_get_property_string (ctx,
												  parent,
												  "usb_device.vendor",
												  NULL);
			*product = g_strdup (halproduct);
			*vendor = g_strdup (halvendor);
			libhal_free_string (halproduct);
			libhal_free_string (halvendor);
			break;
		}
		
		tmp = libhal_device_get_property_string (ctx,
										 parent,
										 "info.parent",
										 NULL);
		libhal_free_string (parent);
		parent = tmp;
	}
	libhal_free_string (parent);	
}

static void
hal_device_added_cb (LibHalContext *ctx, const char *udi)
{
	MBCAAssistantPrivate* priv;
	GtkTreeIter iter;
	gchar* product;
	gchar* vendor;

	/** @todo mutex! */
	
	if (!libhal_device_query_capability (ctx, udi, "modem", NULL))
	{
		return;
	}
	priv = (MBCAAssistantPrivate*)libhal_ctx_get_user_data (ctx);
	
	/* check that device is not already in the list */
	if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->hal_store), &iter))
	{
		do
		{
			gchar* modeludi;
			gtk_tree_model_get (GTK_TREE_MODEL (priv->hal_store), &iter,
							HAL_UDI_COLUMN, &modeludi,
							-1);
			if (g_utf8_collate (modeludi, udi) == 0){
				/* found! */
				g_free (modeludi);
				return;
			}
			g_free (modeludi);
		}
		while (gtk_tree_model_iter_next (GTK_TREE_MODEL (priv->hal_store),
								   &iter));
	}
	
	find_hal_info (ctx, udi, &vendor, &product);
	g_return_if_fail (product);
	
	gtk_list_store_append (priv->hal_store, &iter);
	gtk_list_store_set (priv->hal_store, &iter,
					HAL_PRODUCT_COLUMN, product,
					HAL_VENDOR_COLUMN, vendor,
					HAL_UDI_COLUMN, udi,
					-1);
	g_free (vendor);
	g_free (product);
}

static void
hal_device_new_capability_cb (LibHalContext *ctx,
						const char *udi,
						const char *capability)
{
	if (g_utf8_collate (capability, "modem") == 0)
	{
		hal_device_added_cb (ctx, udi);
	}
}


static void
hal_device_removed_cb (LibHalContext *ctx, const char *udi)
{
	MBCAAssistantPrivate* priv;
	GtkTreeIter iter;
	
	/**
	 * @bug there is a possible race between hal_device_removed_cb()
	 *      and hal_device_added_cb(). This can be solved using a mutex around
	 *      priv->hal_strore, but it's propably not a problem.
	 */
	
	priv = (MBCAAssistantPrivate*)libhal_ctx_get_user_data (ctx);
	if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->hal_store), &iter))
	{
		do
		{
			gchar* modeludi;
			gtk_tree_model_get (GTK_TREE_MODEL (priv->hal_store), &iter,
							HAL_UDI_COLUMN, &modeludi,
							-1);
			if (g_utf8_collate (modeludi, udi) == 0){
				g_free (modeludi);
				gtk_list_store_remove (priv->hal_store, &iter);
				return;
			}
			g_free (modeludi);
		}
		while (gtk_tree_model_iter_next (GTK_TREE_MODEL (priv->hal_store),
								   &iter));
	}	
}

static void
hal_device_lost_capability_cb (LibHalContext *ctx,
						 const char *udi,
						 const char *capability)
{
	if (g_utf8_collate (capability, "modem") == 0)
	{
		hal_device_removed_cb (ctx, udi);
	}
}

/******************************************************************************
 * GObject *
 ***********/


static void
assistant_dispose (GObject *obj __attribute__((unused)))
{
	g_debug ("assistant_dispose");
#if 0
  MamanBar *self = (MamanBar *)obj;

  if (self->priv->dispose_has_run) {
   /* If dispose did already run, return. */
    return;
  }
  /* Make sure dispose does not run twice. */
  object->priv->dispose_has_run = TRUE;

  /* 
   * In dispose, you are supposed to free all types referenced from this
   * object which might themselves hold a reference to self. Generally,
   * the most simple solution is to unref all members on which you own a 
   * reference.
   */

   /* Chain up to the parent class */
   G_OBJECT_CLASS (parent_class)->dispose (obj);
#endif
}

static void
assistant_finalize (GObject *obj)
{
	MBCAAssistant* self = (MBCAAssistant*)obj;
	MBCAAssistantPrivate* priv;
	GtkTreeModel* country_store;
	GtkTreeIter iter;
	
	g_debug ("assistant_finalize");
	priv = self->priv;	

	/* Everything was set to zero in new() by g_malloc0 so any pointer that
	 * is not NULL must be freed
	 */
	
	if (!self->priv)
	{
		return;
	}	
	
	priv->abort_provider_thread = TRUE;
	g_thread_join (priv->provider_thread);	
	g_mutex_free (priv->instances_mutex);
	
	priv->exit_btrescan = TRUE;
	g_thread_join (priv->btrescan_thread);
	

	if (priv->gweather_database)
	{
		g_object_unref (priv->gweather_database);
	}
	
	country_store = GTK_TREE_MODEL (priv->country_store);
	if (country_store)
	{
		if (gtk_tree_model_get_iter_first (country_store, &iter))
		{
			do
			{
				GtkTreeModel* provider_store;
				gtk_tree_model_get (country_store, &iter,
								COUNTRY_PROVIDERS_COLUMN,
								&provider_store,
								-1);
				gtk_list_store_clear (GTK_LIST_STORE (provider_store));
				g_object_unref (provider_store);
			}
			while (gtk_tree_model_iter_next (country_store, &iter));
		}
		gtk_list_store_clear (GTK_LIST_STORE (country_store));
	}	
	
	free_service_provider_database (&(priv->database));	
	
	if (priv->halctx)
	{
		libhal_ctx_shutdown (priv->halctx, NULL);
		libhal_ctx_free (priv->halctx);
	}
	if (priv->halconn)
	{
		dbus_connection_unref(priv->halconn);
	}
	
	mbca_free_configuration (priv->conf);

	g_free (priv->nice_device_name);
	g_free (priv->country_name);
	g_free (priv->provider_name);
	g_free (priv->country_code);
	
	if (priv->assistant)
	{
		gtk_widget_destroy (GTK_WIDGET (priv->assistant));
	}
	
	g_object_unref (priv->builder);
	g_free (priv);	
	
	G_OBJECT_CLASS (parent_class)->finalize (obj);
}

static void
assistant_class_init (gpointer g_class,
                      gpointer g_class_data __attribute__((unused)))
{
	MBCAAssistantClass *klass;
	GObjectClass* gobject_class;
	
	/* translation domain */
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	
	gobject_class = G_OBJECT_CLASS (g_class);
	klass = MBCA_ASSISTANT_CLASS (g_class);
	
	/* set functions */
	gobject_class->dispose = assistant_dispose;
	gobject_class->finalize = assistant_finalize;

	/* store parent class */
	parent_class = g_type_class_peek_parent (klass);

	
	/* private structure */
	klass->priv = g_malloc0 (sizeof (MBCAAssistantClassPrivate));
	
	klass->state_changed_signal_id =
		g_signal_new ("state-changed",
					G_TYPE_FROM_CLASS (g_class),
					G_SIGNAL_RUN_LAST |
					G_SIGNAL_NO_RECURSE |
					G_SIGNAL_NO_HOOKS,
					0, /* class_offset */
					NULL, /* accumulator */
					NULL, /* accu_data */
					g_cclosure_marshal_VOID__ENUM,
					G_TYPE_NONE, /* return_type */
					1, /* n_params */
					MBCA_TYPE_ASSISTANT_STATE); /* param_types */ 
}

static void
assistant_init (GTypeInstance* instance, gpointer g_class __attribute__ ((unused)))
{
	MBCAAssistant* self = (MBCAAssistant*)instance;
	MBCAAssistantPrivate* priv;
	GError* error = NULL;
	GtkTreeIter iter;
	gint ret;	

	/* HAL */
	DBusError halerr;
	int num_devices;
	char** devices;
	char** udi;
	int num;

	
	g_debug ("assistant_init");
	if (!g_thread_supported ())
	{
		g_error ("gthreads are not supported. did you forget to call "
			    "g_thread_init()?"
			    /* "please, RTFM!" */);
	}
	self->priv = g_malloc0 (sizeof (MBCAAssistantPrivate));
	priv = self->priv;
	
	
	/* create list stores */
	priv->baud_store = gtk_list_store_new (SERIALBAUD_N_COLUMNS,
								    G_TYPE_STRING);	    /* rate */
	priv->serial_store = gtk_list_store_new (SERIAL_N_COLUMNS,
									 G_TYPE_STRING,    /* subsystem */
									 G_TYPE_STRING);   /* device	  */
	priv->hal_store = gtk_list_store_new (HAL_N_COLUMNS,
								   G_TYPE_STRING,	 /* product */
								   G_TYPE_STRING,    /* vendor  */
								   G_TYPE_STRING);   /* udi     */	
	priv->country_store = gtk_list_store_new (COUNTRY_N_COLUMNS,
									  G_TYPE_STRING,  /* code      */
									  G_TYPE_STRING,  /* name      */
									  G_TYPE_POINTER, /* providers */
									  G_TYPE_BOOLEAN);/* autosort  */		 	
	
	
	/* HAL connection */
	dbus_error_init (&halerr);
	priv->halconn = dbus_bus_get (DBUS_BUS_SYSTEM, &halerr);
	if(!priv->halconn && dbus_error_is_set (&halerr))
	{
		g_error ("%s: %s\n", halerr.name, halerr.message);
	}
	
	priv->halctx = libhal_ctx_new ();
	g_return_if_fail (priv->halctx);

	if (!libhal_ctx_set_dbus_connection (priv->halctx, priv->halconn))
	{
		g_error ("could not set dbus connection for HAL context");
	}
	if (!libhal_ctx_init (priv->halctx, NULL))
	{
		g_error ("could not initialize HAL context");
	}
	if (!libhal_ctx_set_user_data (priv->halctx, priv))
	{
		g_error ("could not set HAL context user data");
	}		
	
	
	/* HAL callbacks */
	ret = libhal_ctx_set_device_added (priv->halctx, hal_device_added_cb);
	if (!ret)
	{
		g_error ("libhal_ctx_set_device_added failed");
	}

	ret = libhal_ctx_set_device_removed (priv->halctx, hal_device_removed_cb);
	if (!ret)
	{
		g_error ("libhal_ctx_set_device_removed failed");
	}

	ret = libhal_ctx_set_device_new_capability (priv->halctx,
									    hal_device_new_capability_cb);
	if (!ret)
	{
		g_error ("libhal_ctx_set_device_new_capability failed");
	}

	ret = libhal_ctx_set_device_lost_capability (priv->halctx,
										hal_device_lost_capability_cb);
	if (!ret)
	{
		g_error ("libhal_ctx_set_device_lost_capability failed");
	}
	
	
	/* Initial devices for hal_page */
	devices = libhal_get_all_devices (priv->halctx, &num, NULL);
	if (!devices)
	{
		g_debug ("libhal_get_all_devices failed");
	}
	
	for (udi = devices; *udi; udi++)
	{
		if (libhal_device_query_capability (priv->halctx,
									 *udi,
									 "modem",
									 NULL))
		{
			/* using the callback like this leads to concurrency issues */
			hal_device_added_cb (priv->halctx, *udi);
		}
	}
	libhal_free_string_array (devices);
	
	
	/* Initial devices for serial_page
	 * note: serial_page device list is not updated after this.
	 */
	devices = libhal_manager_find_device_string_match (priv->halctx,
											 "info.category",
											 "serial",
											 &num_devices,
											 NULL);
	for (udi = devices; *udi; udi++)
	{
		char* device;
		char* parent;
		char* subsystem;
		
		device = libhal_device_get_property_string (priv->halctx,
										    *udi,
										    "serial.device",
										    NULL);
		parent = libhal_device_get_property_string (priv->halctx,
										    *udi,
										    "serial."
										    "originating_device",
										    NULL);
		subsystem = libhal_device_get_property_string (priv->halctx,
											  parent,
											  "info.subsystem",
											  NULL);		
		
		gtk_list_store_append (priv->serial_store, &iter);
		gtk_list_store_set (priv->serial_store, &iter,
						SERIAL_SUBSYSTEM_COLUMN, subsystem,
						SERIAL_PORT_COLUMN, device,
						-1);
		
		libhal_free_string (device);
		libhal_free_string (parent);
		libhal_free_string (subsystem);
	}
	libhal_free_string_array(devices);
		
	
	/* default baudrates */
	gtk_list_store_append (priv->baud_store, &iter);
	gtk_list_store_set (priv->baud_store, &iter,
					SERIALBAUD_RATE_COLUMN, "115200",
					-1);
	gtk_list_store_append (priv->baud_store, &iter);
	gtk_list_store_set (priv->baud_store, &iter,
					SERIALBAUD_RATE_COLUMN, "57600 ",
					-1);
	gtk_list_store_append (priv->baud_store, &iter);
	gtk_list_store_set (priv->baud_store, &iter,
					SERIALBAUD_RATE_COLUMN, "38400",
					-1);
	gtk_list_store_append (priv->baud_store, &iter);
	gtk_list_store_set (priv->baud_store, &iter,
					SERIALBAUD_RATE_COLUMN, "19200",
					-1);
	gtk_list_store_append (priv->baud_store, &iter);
	gtk_list_store_set (priv->baud_store, &iter,
					SERIALBAUD_RATE_COLUMN, "9600",
					-1);
	
	priv->builder = gtk_builder_new ();
	ret = gtk_builder_add_from_file (priv->builder,
							   MBCA_UI_DEFINITION,
							   &error);
	if (!ret)
	{
		g_error ("could not load the UI: %s", error->message);
	}
	
	
	priv->assistant = GTK_ASSISTANT (gtk_builder_get_object (priv->builder,
												  "mbca_assistant")
							   );
	if (!priv->assistant)
	{
		g_error ("who has b0rk3d the UI file?!");
	}    
	
	
	priv->conf = g_malloc0 (sizeof (MBCAConfiguration));
	
	
	/* do all the GUI initialization before connecting any signals to prevent
	 * unnecessary callbacks
	 */
	setup_bluetooth_page (priv);
	setup_serial_page (priv,
				    priv->serial_store,
				    priv->baud_store);
	setup_hal_page (priv, priv->hal_store);
	setup_provider_page (priv);
	
	gtk_assistant_set_forward_page_func (priv->assistant,
								  mbca_page_func,
								  priv,
								  NULL);	
	connect_signals (self);
	
	
	/* provider resource loading thead */
	/* remember: booleans and pointers are set by g_malloc0 */
	priv->instances_mutex = g_mutex_new ();
	
	priv->trigger_mutex = g_mutex_new ();
	g_mutex_lock (priv->trigger_mutex);

	priv->provider_thread = g_thread_create (mbca_load_provider_page_resources_thread_func,
									priv,
									TRUE,
									NULL);
	if (!priv->provider_thread)
	{
		g_error ("g_thread_create failed");	
	}	
	
	/** @todo fixme 
	 *
	 * if provider thread attaches the resources to assistants, it breaks
	 * the signaling system or something like that. Sometimes provider page
	 * accepts input, but is drawn as insensitive, and sometimes country
	 * expander appears as empty, and sometimes assistant buttons act all
	 * wierd. 
	 */
#if 0	
	/*
	 * waiting_instances is a GSList of MBCAAssistantPrivate structs for
	 * every instance waiting for the data. When provider thread is done it
	 * will automaticly go through the list and attach the data.
	 *
	 * provider_thread_ready or waiting_instances must not be accessed if 
	 * instances_mutex is not locked!
	 */
	g_mutex_lock (klass->priv->instances_mutex);
	if (klass->priv->provider_thread_ready)
	{
		g_debug ("assistant_init: attaching resources");
		/* thread is done so we can attach the resources right away */
		mbca_provider_attach_resources (klass->priv, priv);
	}
	else
	{
		g_debug ("assistant_init: adding instance to resource wait queue");
		/* thread is still running so lets hook up for wait queue and let
		 * thread handle the attaching
		 */
		
		GSList** list = &klass->priv->waiting_instances;
		*list = g_slist_prepend (*list, priv);
	}
	g_mutex_unlock (klass->priv->instances_mutex);
#endif	
	
	/* everything else in MBCAAssistantPrivate is safely set to NULL, FALSE,
	 * etc by g_malloc0
	 */
}

/******************************************************************************
 * Public API *
 **************/

void
mbca_free_configuration (MBCAConfiguration* configuration)
{
	if (!configuration)
	{
		return;
	}
	
	free_serviceprovider (configuration->provider);
	g_free (configuration->baud);
	g_free (configuration->device);
	g_free (configuration->name);
	g_free (configuration);
}

GType
mbca_assistant_get_type ()
{
	static GType type = 0;
	if (type == 0) {
		
		static const GTypeInfo info = {
			sizeof (MBCAAssistantClass),
			NULL,   /* base_init */
			NULL,   /* base_finalize */
			assistant_class_init,
			NULL,   /* class_finalize */
			NULL,   /* class_data */
			sizeof (MBCAAssistant),
			0,      /* n_preallocs */
			assistant_init,
			NULL    /* value_table */
		};
		type = g_type_register_static (G_TYPE_OBJECT,
								 "MBCAAssistantType",
								 &info, 0);
	}
	return type;
}

MBCAAssistant*
mbca_assistant_new ()
{
	return g_object_new (mbca_assistant_get_type(), NULL);
}

gint
mbca_assistant_run (MBCAAssistant* assistant)
{
	return mbca_assistant_run_for_device (assistant,
								   MBCA_DEVICE_NONE,
								   NULL, NULL);
}

gint
mbca_assistant_run_for_device (MBCAAssistant* assistant,
						 MBCADeviceType type,
						 const gchar* device,
						 const gchar* nice_device_name)
{
	MBCAAssistantPrivate* priv = assistant->priv;
	
	if (priv->dispose_has_run)
	{
	  /* Dispose has run. Data is not valid anymore. */
	  return 0;
	}

	g_return_val_if_fail (priv->state == MBCA_STATE_READY, -1);
	
	priv->state = MBCA_STATE_RUNNING;
	g_signal_emit (assistant,
				MBCA_ASSISTANT_GET_CLASS (assistant)->state_changed_signal_id,
				0,
				priv->state);
	
	
	/* default baud rate */
	priv->conf->baud = g_strdup ("115200");
	
	
	if (type != MBCA_DEVICE_NONE)
	{
		priv->device_is_preset = TRUE;
		priv->conf->device = g_strdup (device);
		priv->nice_device_name = g_strdup (nice_device_name);
		priv->conf->type = type;
		switch (type)
		{
			case MBCA_DEVICE_SERIAL:
			{	
				/* serial devices are special.
				 * device name _is_ the nice name and baud is stored 
				 * in nice_device_name
				 */
				
				g_free (priv->nice_device_name);
				priv->nice_device_name = g_strdup (device);
				
				g_free (priv->conf->baud);
				priv->conf->baud = g_strdup (nice_device_name);
				break;			
			}
			case MBCA_DEVICE_BLUETOOTH:
			case MBCA_DEVICE_HAL:
			case MBCA_DEVICE_PSEUDO:
			{
				break;
			}
			default:
			{
				g_return_val_if_reached (-1);
			}
		}
	}
	
	set_method_radio_buttons_inactive (priv);
	gtk_widget_show (GTK_WIDGET (priv->assistant));	
	
	return 0;
}
				 
MBCAAssistantState
mbca_assistant_get_state (MBCAAssistant* assistant)
{
	if (assistant->priv->dispose_has_run)
	{
	  /* Dispose has run. Data is not valid anymore. */
	  return MBCA_STATE_ABORTED;
	}
	
	return assistant->priv->state;
}

MBCAConfiguration*
mbca_assistant_get_configuration (MBCAAssistant* assistant)
{
	MBCAConfiguration* newconf;
	MBCAConfiguration* conf;
	
	if (assistant->priv->dispose_has_run)
	{
	  /* Dispose has run. Data is not valid anymore. */
	  return NULL;
	}
	
	g_return_val_if_fail (assistant->priv->state == MBCA_STATE_DONE, NULL);

	/* duplicate */
	conf = assistant->priv->conf;
	newconf = g_malloc0 (sizeof (MBCAConfiguration));
	newconf->name = g_strdup (conf->name);
	newconf->type = conf->type;
	newconf->device = g_strdup (conf->device);
	newconf->baud = g_strdup (conf->baud);
	newconf->provider = duplicate_serviceprovider (conf->provider);
	
	return newconf;
}

void
mbca_assistant_abort (MBCAAssistant* assistant)
{
	if (assistant->priv->dispose_has_run)
	{
	  /* Dispose has run. Data is not valid anymore. */
	  return;
	}
	
	gtk_widget_hide (GTK_WIDGET (assistant->priv->assistant));
	mbca_assistant_cancel_cb(NULL, assistant);
}

void
mbca_assistant_present (MBCAAssistant* assistant)
{
	g_return_if_fail (assistant->priv->state == MBCA_STATE_RUNNING);

	gtk_window_present (GTK_WINDOW (assistant->priv->assistant));
	return;
}

/*******************************************************************************
 * Assistant callbacks *
 ***********************/

void
mbca_assistant_apply_cb (GtkAssistant* gtkassistant __attribute__ ((unused)),
					gpointer user_data)
{
	MBCAAssistant* assistant = user_data;
	MBCAAssistantPrivate* priv = assistant->priv;
	
	priv->done = TRUE;
	priv->state = MBCA_STATE_DONE;
	
	/* let close_cb() handle signal emission */
}

void
mbca_assistant_cancel_cb (GtkButton* button,
					 gpointer user_data)
{
	MBCAAssistant* assistant = user_data;
	MBCAAssistantPrivate* priv = assistant->priv;
	
	priv->btrescan_enabled = FALSE; /* make sure scan thread goes silent */
	
	priv->state = MBCA_STATE_ABORTED;
	mbca_assistant_close_cb (button, user_data);
}

void
mbca_assistant_close_cb (GtkButton* button __attribute__ ((unused)),
					gpointer user_data)
{
	MBCAAssistant* assistant = user_data;
	MBCAAssistantPrivate* priv = assistant->priv;

	gtk_widget_hide (GTK_WIDGET (priv->assistant));
	
	g_signal_emit (assistant,
				MBCA_ASSISTANT_GET_CLASS (assistant)->state_changed_signal_id,
				0,
				priv->state);
}

void
mbca_assistant_prepare_cb (GtkAssistant* gtkassistant,
					  GtkWidget* page,
					  gpointer user_data)
{
	MBCAAssistant* assistant = user_data;
	MBCAAssistantPrivate* priv = assistant->priv;
	gint page_number = mbca_page_name_to_number (gtk_widget_get_name (page));
	
	if (page_number == PAGE_BLUETOOTH)
	{
		priv->btrescan_enabled = TRUE;
		/* also take a look at mbca_bluetooth_radiobutton_clicked_cb () */
	}
	else
	{
		priv->btrescan_enabled = FALSE;
	}
	
	switch(page_number)
	{
		case PAGE_INTRO:
		{
			break;
		}
		case PAGE_METHOD:
		{
			mbca_method_page_prepare_cb (gtkassistant, page, user_data);
			break;
		}
		case PAGE_BLUETOOTH:
		{
			mbca_bluetooth_page_prepare_cb (gtkassistant, page, user_data);
			break;
		}
		case PAGE_SERIAL:
		{
			mbca_serial_page_prepare_cb (gtkassistant, page, user_data);
			break;
		}
		case PAGE_HAL:
		{
			mbca_hal_page_prepare_cb (gtkassistant, page, user_data);
			break;
		}
		case PAGE_PROVIDER:
		{
			mbca_provider_page_prepare_cb (gtkassistant, page, user_data);
			break;
		}
		case PAGE_SUMMARY:
		{
			mbca_summary_page_prepare_cb (gtkassistant, page, user_data);
			break;
		}
		default:
		{
			g_warn_if_reached();
		}	
	}
} 

/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file mbca_serviceprovider_private.h
 * @brief private API
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * This file defines the private API for service provider operations.
 */

#ifndef MBCA_SERVICEPROVIDER_PRIVATE_H
#define MBCA_SERVICEPROVIDER_PRIVATE_H

#include "mbca_serviceprovider.h"

G_BEGIN_DECLS

MBCAServiceProvider*
duplicate_serviceprovider (MBCAServiceProvider* provider);

void
free_serviceprovider (MBCAServiceProvider* provider);

G_END_DECLS

#endif /* MBCA_SERVICEPROVIDER_PRIVATE_H */

 

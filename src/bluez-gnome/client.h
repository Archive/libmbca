/*
 *
 *  BlueZ - Bluetooth protocol stack for Linux
 *
 *  Copyright (C) 2005-2008  Marcel Holtmann <marcel@holtmann.org>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef __BLUETOOTH_CLIENT_H
#define __BLUETOOTH_CLIENT_H

#include <glib-object.h>

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLUETOOTH_TYPE_CLIENT (bluetooth_client_get_type())
#define BLUETOOTH_CLIENT(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), \
					BLUETOOTH_TYPE_CLIENT, BluetoothClient))
#define BLUETOOTH_CLIENT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), \
					BLUETOOTH_TYPE_CLIENT, BluetoothClientClass))
#define BLUETOOTH_IS_CLIENT(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), \
							BLUETOOTH_TYPE_CLIENT))
#define BLUETOOTH_IS_CLIENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), \
							BLUETOOTH_TYPE_CLIENT))
#define BLUETOOTH_GET_CLIENT_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), \
					BLUETOOTH_TYPE_CLIENT, BluetoothClientClass))

typedef struct _BluetoothClient BluetoothClient;
typedef struct _BluetoothClientClass BluetoothClientClass;

struct _BluetoothClient {
	GObject parent;
};

struct _BluetoothClientClass {
	GObjectClass parent_class;

	void (*discovery_started) (BluetoothClient *self, const char *adapter_path, gboolean is_default);
	void (*discovery_completed) (BluetoothClient *self, const char *adapter_path, gboolean is_default);
};

GType bluetooth_client_get_type(void);

BluetoothClient *bluetooth_client_new(void);

enum {
	COLUMN_PATH,
	COLUMN_ACTIVE,
	COLUMN_OBJECT,
	COLUMN_ADDRESS,
	COLUMN_CLASS,
	COLUMN_RSSI,
	COLUMN_NAME,
	COLUMN_TYPE,
	COLUMN_BONDED,
	COLUMN_TRUSTED,
	COLUMN_CONNECTED,
	COLUMN_DISCOVERING,
	NUM_COLS
};

enum {
	BLUETOOTH_TYPE_ANY        = 1,
	BLUETOOTH_TYPE_PHONE      = 1 << 1,
	BLUETOOTH_TYPE_MODEM      = 1 << 2,
	BLUETOOTH_TYPE_COMPUTER   = 1 << 3,
	BLUETOOTH_TYPE_NETWORK    = 1 << 4,
	BLUETOOTH_TYPE_HEADSET    = 1 << 5,
	BLUETOOTH_TYPE_KEYBOARD   = 1 << 6,
	BLUETOOTH_TYPE_MOUSE      = 1 << 7,
	BLUETOOTH_TYPE_CAMERA     = 1 << 8,
	BLUETOOTH_TYPE_PRINTER    = 1 << 9,
	BLUETOOTH_TYPE_JOYPAD     = 1 << 10,
	BLUETOOTH_TYPE_TABLET     = 1 << 11,
};

#define BLUETOOTH_TYPE_NUM_TYPES 12
#define BLUETOOTH_TYPE_INPUT (BLUETOOTH_TYPE_KEYBOARD | BLUETOOTH_TYPE_MOUSE)

const gchar *bluetooth_type_to_string(guint type);

gboolean bluetooth_client_register_passkey_agent(BluetoothClient *self,
		const char *path, const char *address, const void *info);

gboolean bluetooth_client_create_bonding(BluetoothClient *self,
					gchar *adapter, const gchar *address);
gboolean bluetooth_client_remove_bonding(BluetoothClient *self,
					gchar *adapter, const gchar *address);
gboolean bluetooth_client_set_trusted(BluetoothClient *self,
					gchar *adapter, const gchar *address);
gboolean bluetooth_client_remove_trust(BluetoothClient *self,
					gchar *adapter, const gchar *address);

gboolean bluetooth_client_disconnect(BluetoothClient *self,
					gchar *adapter, const gchar *address);

gboolean bluetooth_client_discover_devices(BluetoothClient *self, gchar *adapter);

gboolean bluetooth_client_cancel_discovery(BluetoothClient *self, gchar *adapter);

GtkTreeModel *bluetooth_client_get_model(BluetoothClient *self);
GtkTreeModel *bluetooth_client_get_model_simple(BluetoothClient *self);
GtkTreeModel *bluetooth_client_get_model_adapter_list(BluetoothClient *self);
GtkTreeModel *bluetooth_client_get_model_for_adapter(BluetoothClient *self,
								gchar *adapter);
GtkTreeModel *bluetooth_client_get_model_with_filter(BluetoothClient *self,
			gchar *adapter, GtkTreeModelFilterVisibleFunc func,
								gpointer data);
GtkTreeModel *bluetooth_client_get_model_bonded_list(BluetoothClient *self,
								gchar *adapter);

G_END_DECLS

#endif /* __BLUETOOTH_CLIENT_H */

/*
 *
 *  BlueZ - Bluetooth protocol stack for Linux
 *
 *  Copyright (C) 2005-2008  Marcel Holtmann <marcel@holtmann.org>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>

#include <dbus/dbus-glib.h>

#include "client.h"

#include "marshal.h"

#include "dbus-glue.h"

static BluetoothClient *self = NULL;

#define BLUETOOTH_CLIENT_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), \
				BLUETOOTH_TYPE_CLIENT, BluetoothClientPrivate))

typedef struct _BluetoothClientPrivate BluetoothClientPrivate;

struct _BluetoothClientPrivate {
	gboolean registered;

	DBusGConnection *conn;
	DBusGProxy *manager_object;
	GtkTreeStore *store;
	gchar *default_adapter;
};

enum {
	DISCOVERY_STARTED,
	DISCOVERY_COMPLETED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_DEFAULT_ADAPTER
};

static int client_table_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE(BluetoothClient, bluetooth_client, G_TYPE_OBJECT)

static gboolean find_iter_for_object(DBusGProxy *object,
				     BluetoothClient *client,
				     GtkTreeIter *iter)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	gboolean cont;

	if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), iter) == FALSE)
		return FALSE;

	cont = TRUE;
	while (cont == TRUE) {
		DBusGProxy *o;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), iter,
						COLUMN_OBJECT, &o, -1);
		if (o == object)
			return TRUE;
		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), iter);
	}

	return FALSE;
}

static void timeout_changed(DBusGProxy *object,
				const guint32 timeout, gpointer user_data)
{
}

static void name_changed(DBusGProxy *object,
				const char *name, gpointer user_data)
{
	BluetoothClient *client = BLUETOOTH_CLIENT(user_data);
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);

	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(client), &iter) == FALSE)
		return;

	gtk_tree_store_set(priv->store, &iter, COLUMN_NAME, name, -1);
}

static void minor_changed(DBusGProxy *object,
				const char *minor, gpointer user_data)
{
}

const gchar *bluetooth_type_to_string(guint type)
{
	switch (type) {
	case BLUETOOTH_TYPE_ANY:
		return N_("All types");
	case BLUETOOTH_TYPE_PHONE:
		return N_("Phone");
	case BLUETOOTH_TYPE_MODEM:
		return N_("Modem");
	case BLUETOOTH_TYPE_COMPUTER:
		return N_("Computer");
	case BLUETOOTH_TYPE_NETWORK:
		return N_("Network");
	case BLUETOOTH_TYPE_HEADSET:
		return N_("Headset");
	case BLUETOOTH_TYPE_KEYBOARD:
		return N_("Keyboard");
	case BLUETOOTH_TYPE_MOUSE:
		return N_("Mouse");
	case BLUETOOTH_TYPE_CAMERA:
		return N_("Camera");
	case BLUETOOTH_TYPE_PRINTER:
		return N_("Printer");
	case BLUETOOTH_TYPE_JOYPAD:
		return N_("Joypad");
	case BLUETOOTH_TYPE_TABLET:
		return N_("Tablet");
	default:
		return N_("Unknown");
	}
}

static guint class_to_type(guint32 class)
{
	switch ((class & 0x1f00) >> 8) {
	case 0x01:
		return BLUETOOTH_TYPE_COMPUTER;
	case 0x02:
		switch ((class & 0xfc) >> 2) {
		case 0x01:
		case 0x02:
		case 0x03:
		case 0x05:
			return BLUETOOTH_TYPE_PHONE;
		case 0x04:
			return BLUETOOTH_TYPE_MODEM;
		}
		break;
	case 0x03:
		return BLUETOOTH_TYPE_NETWORK;
	case 0x04:
		switch ((class & 0xfc) >> 2) {
		case 0x01:
		case 0x02:
			return BLUETOOTH_TYPE_HEADSET;
		}
		break;
	case 0x05:
		switch ((class & 0xc0) >> 6) {
		case 0x00:
			switch ((class & 0x1e) >> 2) {
			case 0x01:
			case 0x02:
				return BLUETOOTH_TYPE_JOYPAD;
			}
			break;
		case 0x01:
			return BLUETOOTH_TYPE_KEYBOARD;
		case 0x02:
			switch ((class & 0x1e) >> 2) {
			case 0x05:
				return BLUETOOTH_TYPE_TABLET;
			default:
				return BLUETOOTH_TYPE_MOUSE;
			}
		}
		break;
	case 0x06:
		if (class & 0x80)
			return BLUETOOTH_TYPE_PRINTER;
		if (class & 0x20)
			return BLUETOOTH_TYPE_CAMERA;
		break;
	}

	return BLUETOOTH_TYPE_ANY;
}

static void insert_device(DBusGProxy *object, GtkTreeIter *parent,
			BluetoothClient *client, const char *address,
			const int rssi, gboolean active, gboolean lookup)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	GHashTable *hash = NULL;
	GValue *value;
	const char *name = NULL;
	guint type;
	guint32 class = 0;
	gboolean bonded = FALSE, trusted = FALSE, connected = FALSE;
	gboolean cont;

	dbus_g_proxy_call(object, "GetRemoteInfo", NULL,
				G_TYPE_STRING, address, G_TYPE_INVALID,
				dbus_g_type_get_map("GHashTable",
						G_TYPE_STRING, G_TYPE_VALUE),
				&hash, G_TYPE_INVALID);

	value = (hash == NULL) ? NULL : g_hash_table_lookup(hash, "name");
	if (value == NULL) {
		if (lookup) {
			dbus_g_proxy_call(object, "GetRemoteName", NULL,
				  G_TYPE_STRING, address, G_TYPE_INVALID,
				  G_TYPE_STRING, &name, G_TYPE_INVALID);
		} else
			name = g_strdup(address);
	} else
		name = g_value_get_string(value);

	value = (hash == NULL) ? NULL : g_hash_table_lookup(hash, "class");
	if (value == NULL) {
		if (lookup) {
			dbus_g_proxy_call(object, "GetRemoteClass", NULL,
				  G_TYPE_STRING, address, G_TYPE_INVALID,
				  G_TYPE_UINT, &class, G_TYPE_INVALID);
		}
	} else
		class = g_value_get_uint(value);

	value = (hash == NULL) ? NULL : g_hash_table_lookup(hash, "bonded");
	if (value == NULL)
		dbus_g_proxy_call(object, "HasBonding", NULL,
				G_TYPE_STRING, address, G_TYPE_INVALID,
				G_TYPE_BOOLEAN, &bonded, G_TYPE_INVALID);
	else
		bonded = g_value_get_boolean(value);

	value = (hash == NULL) ? NULL : g_hash_table_lookup(hash, "trusted");
	if (value == NULL)
		dbus_g_proxy_call(object, "IsTrusted", NULL,
				G_TYPE_STRING, address, G_TYPE_INVALID,
				G_TYPE_BOOLEAN, &trusted, G_TYPE_INVALID);
	else
		trusted = g_value_get_boolean(value);

	value = (hash == NULL) ? NULL : g_hash_table_lookup(hash, "connected");
	if (value == NULL)
		dbus_g_proxy_call(object, "IsConnected", NULL,
				G_TYPE_STRING, address, G_TYPE_INVALID,
				G_TYPE_BOOLEAN, &connected, G_TYPE_INVALID);
	else
		connected = g_value_get_boolean(value);

	cont = gtk_tree_model_iter_children(GTK_TREE_MODEL(priv->store),
							&iter, parent);

	type = class_to_type(class);

	while (cont == TRUE) {
		gchar *value;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_ADDRESS, &value, -1);

		if (g_ascii_strcasecmp(address, value) == 0) {
			gtk_tree_store_set(priv->store, &iter,
						COLUMN_ACTIVE, active,
						COLUMN_CLASS, class,
						COLUMN_RSSI, rssi,
						COLUMN_NAME, name,
						COLUMN_TYPE, type,
						COLUMN_BONDED, bonded,
						COLUMN_TRUSTED, trusted,
						COLUMN_CONNECTED, connected,
						COLUMN_DISCOVERING, FALSE, -1);
			return;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	gtk_tree_store_insert_with_values(priv->store, &iter, parent, -1,
					COLUMN_ACTIVE, active,
					COLUMN_ADDRESS, address,
					COLUMN_CLASS, class,
					COLUMN_RSSI, rssi,
					COLUMN_NAME, name,
					COLUMN_TYPE, type,
					COLUMN_BONDED, bonded,
					COLUMN_TRUSTED, trusted,
					COLUMN_CONNECTED, connected,
					COLUMN_DISCOVERING, FALSE, -1);
}

static void invalidate_device(DBusGProxy *object,
				GtkTreeIter *parent,
				BluetoothClient *client,
				const char *address)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	cont = gtk_tree_model_iter_children(GTK_TREE_MODEL(priv->store),
							&iter, parent);

	while (cont == TRUE) {
		gchar *value;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_ADDRESS, &value, -1);

		if (g_ascii_strcasecmp(address, value) == 0) {
			gtk_tree_store_set(priv->store, &iter,
						COLUMN_ACTIVE, FALSE, -1);
			return;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}
}

static void change_connected(DBusGProxy *object, GtkTreeIter *parent,
			        BluetoothClient *client,
				const char *address, gboolean connected)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	cont = gtk_tree_model_iter_children(GTK_TREE_MODEL(priv->store),
							&iter, parent);

	while (cont == TRUE) {
		gchar *value;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_ADDRESS, &value, -1);

		if (g_ascii_strcasecmp(address, value) == 0) {
			gtk_tree_store_set(priv->store, &iter,
					COLUMN_CONNECTED, connected, -1);
			return;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}
}

static void rename_device(DBusGProxy *object, GtkTreeIter *parent,
					BluetoothClient *client,
					const char *address, const char *name)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	cont = gtk_tree_model_iter_children(GTK_TREE_MODEL(priv->store),
							&iter, parent);

	while (cont == TRUE) {
		gchar *value;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_ADDRESS, &value, -1);

		if (g_ascii_strcasecmp(address, value) == 0) {
			gtk_tree_store_set(priv->store, &iter,
						COLUMN_NAME, name, -1);
			return;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}
}

static void change_bonded(DBusGProxy *object, GtkTreeIter *parent,
					BluetoothClient *client,
					const char *address, gboolean bonded)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	cont = gtk_tree_model_iter_children(GTK_TREE_MODEL(priv->store),
							&iter, parent);

	while (cont == TRUE) {
		gchar *value;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_ADDRESS, &value, -1);

		if (g_ascii_strcasecmp(address, value) == 0) {
			gtk_tree_store_set(priv->store, &iter,
						COLUMN_BONDED, bonded, -1);
			return;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}
}

static void change_trusted(DBusGProxy *object, GtkTreeIter *parent,
					BluetoothClient *client,
					const char *address, gboolean trusted)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	cont = gtk_tree_model_iter_children(GTK_TREE_MODEL(priv->store),
							&iter, parent);

	while (cont == TRUE) {
		gchar *value;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_ADDRESS, &value, -1);

		if (g_ascii_strcasecmp(address, value) == 0) {
			gtk_tree_store_set(priv->store, &iter,
						COLUMN_TRUSTED, trusted, -1);
			return;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}
}

static void device_found(DBusGProxy *object, const char *address,
		const unsigned int class, const int rssi, gpointer user_data)
{
	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(user_data), &iter) == FALSE)
		return;

	insert_device(object, &iter, BLUETOOTH_CLIENT(user_data),
						address, rssi, TRUE, TRUE);
}

static void device_disappeared(DBusGProxy *object,
				const char *address, gpointer user_data)
{
	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(user_data), &iter) == FALSE)
		return;

	invalidate_device(object, &iter, BLUETOOTH_CLIENT(user_data), address);
}

static void device_connected(DBusGProxy *object,
				const char *address, gpointer user_data)
{
	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(user_data), &iter) == FALSE)
		return;

	change_connected(object, &iter, BLUETOOTH_CLIENT(user_data), address, TRUE);
}

static void device_disconnected(DBusGProxy *object,
				const char *address, gpointer user_data)
{
	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(user_data), &iter) == FALSE)
		return;

	change_connected(object, &iter, BLUETOOTH_CLIENT(user_data), address, FALSE);
}

static void name_updated(DBusGProxy *object,
		const char *address, const char *name, gpointer user_data)
{
	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(user_data), &iter) == FALSE)
		return;

	rename_device(object, &iter, BLUETOOTH_CLIENT(user_data), address, name);
}

static void bonding_created(DBusGProxy *object,
				const char *address, gpointer user_data)
{
	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(user_data), &iter) == FALSE)
		return;

	change_bonded(object, &iter, BLUETOOTH_CLIENT(user_data), address, TRUE);
}

static void bonding_removed(DBusGProxy *object,
				const char *address, gpointer user_data)
{
	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(user_data), &iter) == FALSE)
		return;

	change_bonded(object, &iter, BLUETOOTH_CLIENT(user_data), address, FALSE);
}

static void trust_added(DBusGProxy *object,
				const char *address, gpointer user_data)
{
	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(user_data), &iter) == FALSE)
		return;

	change_trusted(object, &iter, BLUETOOTH_CLIENT(user_data), address, TRUE);
}

static void trust_removed(DBusGProxy *object,
				const char *address, gpointer user_data)
{
	GtkTreeIter iter;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(user_data), &iter) == FALSE)
		return;

	change_trusted(object, &iter, BLUETOOTH_CLIENT(user_data), address, FALSE);
}

static void update_adapter(DBusGProxy *object, GtkTreeIter *iter, BluetoothClient *client)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GError *error = NULL;
	char **array = NULL;
	char *address = NULL, *mode = NULL;
	char *name = NULL, *major = NULL, *minor = NULL;
	const guint32 timeout;

	adapter_get_address(object, &address, NULL);

	adapter_get_mode(object, &mode, NULL);

	dbus_g_proxy_call(object, "GetDiscoverableTimeout", NULL, G_TYPE_INVALID,
				G_TYPE_UINT, &timeout, G_TYPE_INVALID);

	adapter_get_name(object, &name, NULL);

	adapter_get_major_class(object, &major, NULL);

	adapter_get_minor_class(object, &minor, NULL);

	gtk_tree_store_set(priv->store, iter, COLUMN_ADDRESS, address,
						COLUMN_NAME, name, -1);

	adapter_list_connections(object, &array, &error);

	if (error == NULL) {
		while (*array) {
			insert_device(object, iter, client,
						*array, 0, FALSE, TRUE);
			array++;
		}
	} else {
		g_error_free(error);
		error = NULL;
		goto failover;
	}

	adapter_list_bondings(object, &array, &error);

	if (error == NULL) {
		while (*array) {
			insert_device(object, iter, client,
						*array, 0, FALSE, FALSE);
			array++;
		}
	} else {
		g_error_free(error);
		error = NULL;
		goto failover;
	}

	adapter_list_trusts(object, &array, &error);

	if (error == NULL) {
		while (*array) {
			insert_device(object, iter, client,
						*array, 0, FALSE, FALSE);
			array++;
		}
	} else {
		g_error_free(error);
		error = NULL;
		goto failover;
	}

failover:
	adapter_list_remote_devices(object, &array, &error);

	if (error == NULL) {
		while (*array) {
			insert_device(object, iter, client,
						*array, 0, FALSE, FALSE);
			array++;
		}
	} else
		g_error_free(error);
}

static void mode_changed(DBusGProxy *object,
				const char *mode, gpointer user_data)
{
	BluetoothClient *client = (BluetoothClient *) user_data;
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		DBusGProxy *o;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_OBJECT, &o, -1);

		if (object == o) {
			gtk_tree_store_set(priv->store, &iter,
						COLUMN_ACTIVE, TRUE, -1);
			update_adapter(object, &iter, client);

			return;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}
}

static void discovery_event(DBusGProxy *object,
			    BluetoothClient *client,
			    gboolean starting)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	char *adapter;
	gboolean is_default;

	if (find_iter_for_object(object, BLUETOOTH_CLIENT(client), &iter) == FALSE)
		return;

	gtk_tree_store_set(priv->store, &iter, COLUMN_DISCOVERING, starting, -1);

	/* Find out whether we're checking the default adapter */
	is_default = FALSE;
	gtk_tree_model_get (GTK_TREE_MODEL (priv->store), &iter, COLUMN_PATH, &adapter, -1);
	if (priv->default_adapter != NULL) {
		if (g_str_equal (priv->default_adapter, adapter) != FALSE)
			is_default = TRUE;
	}

	if (starting)
		g_signal_emit(G_OBJECT(client), client_table_signals[DISCOVERY_STARTED],
			      0, adapter, is_default);
	else
		g_signal_emit(G_OBJECT(client), client_table_signals[DISCOVERY_COMPLETED],
			      0, adapter, is_default);

	g_free (adapter);
}

static void discovery_completed(DBusGProxy *object,
				gpointer user_data)
{
	BluetoothClient *client = BLUETOOTH_CLIENT(user_data);

	discovery_event (object, client, FALSE);
}

static void discovery_started(DBusGProxy *object,
			      gpointer user_data)
{
	BluetoothClient *client = BLUETOOTH_CLIENT(user_data);

	discovery_event (object, client, TRUE);
}

static void add_adapter(const char *path, BluetoothClient *client)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	DBusGProxy *object;
	GtkTreeIter iter;
	gboolean cont;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		gchar *value;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &value,
						COLUMN_OBJECT, &object, -1);

		if (g_ascii_strcasecmp(path, value) == 0) {
			gtk_tree_store_set(priv->store, &iter,
						COLUMN_ACTIVE, TRUE, -1);

			update_adapter(object, &iter, client);

			return;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	object = dbus_g_proxy_new_for_name(priv->conn, "org.bluez",
						path, "org.bluez.Adapter");

	gtk_tree_store_insert_with_values(priv->store, &iter, NULL, -1,
						COLUMN_PATH, path,
						COLUMN_ACTIVE, TRUE,
						COLUMN_OBJECT, object, -1);

	g_object_unref(object);

	dbus_g_proxy_add_signal(object, "ModeChanged",
					G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "ModeChanged",
				G_CALLBACK(mode_changed), client, NULL);

	dbus_g_proxy_add_signal(object, "DiscoverableTimeoutChanged",
					G_TYPE_UINT, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "DiscoverableTimeoutChanged",
				G_CALLBACK(timeout_changed), client, NULL);

	dbus_g_proxy_add_signal(object, "NameChanged",
					G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "NameChanged",
				G_CALLBACK(name_changed), client, NULL);

	dbus_g_proxy_add_signal(object, "MinorClassChanged",
					G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "MinorClassChanged",
				G_CALLBACK(minor_changed), client, NULL);

	dbus_g_proxy_add_signal(object, "RemoteDeviceFound",
						G_TYPE_STRING, G_TYPE_UINT,
						G_TYPE_INT, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "RemoteDeviceFound",
				G_CALLBACK(device_found), client, NULL);

	dbus_g_proxy_add_signal(object, "RemoteDeviceDisappeared",
						G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "RemoteDeviceDisappeared",
				G_CALLBACK(device_disappeared), client, NULL);

	dbus_g_proxy_add_signal(object, "RemoteDeviceConnected",
						G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "RemoteDeviceConnected",
				G_CALLBACK(device_connected), client, NULL);

	dbus_g_proxy_add_signal(object, "RemoteDeviceDisconnected",
						G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "RemoteDeviceDisconnected",
				G_CALLBACK(device_disconnected), client, NULL);

	dbus_g_proxy_add_signal(object, "RemoteNameUpdated",
				G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "RemoteNameUpdated",
				G_CALLBACK(name_updated), client, NULL);

	dbus_g_proxy_add_signal(object, "BondingCreated",
						G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "BondingCreated",
				G_CALLBACK(bonding_created), client, NULL);

	dbus_g_proxy_add_signal(object, "BondingRemoved",
						G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "BondingRemoved",
				G_CALLBACK(bonding_removed), client, NULL);

	dbus_g_proxy_add_signal(object, "TrustAdded",
						G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "TrustAdded",
				G_CALLBACK(trust_added), client, NULL);

	dbus_g_proxy_add_signal(object, "TrustRemoved",
						G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "TrustRemoved",
				G_CALLBACK(trust_removed), client, NULL);

	dbus_g_proxy_add_signal(object, "DiscoveryStarted", G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "DiscoveryStarted",
				G_CALLBACK(discovery_started), client, NULL);

	dbus_g_proxy_add_signal(object, "DiscoveryCompleted", G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "DiscoveryCompleted",
				G_CALLBACK(discovery_completed), client, NULL);

	update_adapter(object, &iter, client);
}

static void remove_adapter(const char *path, BluetoothClient *client)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		gchar *value;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &value, -1);

		if (g_ascii_strcasecmp(path, value) == 0) {
			GtkTreeIter child;

			gtk_tree_store_set(priv->store, &iter,
						COLUMN_ACTIVE, FALSE,
						COLUMN_ADDRESS, NULL,
						COLUMN_NAME, NULL, -1);

			/* Remove the children */
			if (gtk_tree_model_iter_children(GTK_TREE_MODEL(priv->store),
							 &child, &iter) == FALSE)
				return;

			cont = gtk_tree_store_remove (priv->store, &child);
			while (cont != FALSE)
				cont = gtk_tree_store_remove (priv->store, &child);
			return;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}
}

static void adapter_added(DBusGProxy *object,
				const char *path, gpointer user_data)
{
	add_adapter(path, BLUETOOTH_CLIENT(user_data));
}

static void adapter_removed(DBusGProxy *object,
				const char *path, gpointer user_data)
{
	remove_adapter(path, BLUETOOTH_CLIENT(user_data));
}

static void default_adapter_changed(DBusGProxy *object,
				const char *path, gpointer user_data)
{
	BluetoothClient *client = (BluetoothClient *) user_data;
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);

	g_free(priv->default_adapter);
	if (path != NULL && *path != '\0')
		priv->default_adapter = g_strdup(path);
	else
		priv->default_adapter = NULL;

	g_object_notify (G_OBJECT (client), "default-adapter");
}

static void setup_manager(BluetoothClient *client)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GError *error = NULL;
	char **array = NULL;

	priv->manager_object = dbus_g_proxy_new_for_name(priv->conn, "org.bluez",
					"/org/bluez", "org.bluez.Manager");

	dbus_g_proxy_add_signal(priv->manager_object, "AdapterAdded",
					G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(priv->manager_object, "AdapterAdded",
				G_CALLBACK(adapter_added), client, NULL);

	dbus_g_proxy_add_signal(priv->manager_object, "AdapterRemoved",
					G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(priv->manager_object, "AdapterRemoved",
				G_CALLBACK(adapter_removed), client, NULL);

	dbus_g_proxy_add_signal(priv->manager_object, "DefaultAdapterChanged",
					G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(priv->manager_object, "DefaultAdapterChanged",
			G_CALLBACK(default_adapter_changed), client, NULL);

	manager_default_adapter(priv->manager_object, &priv->default_adapter, NULL);

	manager_list_adapters(priv->manager_object, &array, &error);

	if (error == NULL) {
		while (*array) {
			add_adapter(*array, client);
			array++;
		}
	} else
		g_error_free(error);
}

static void name_owner_changed(DBusGProxy *object, const char *name,
			const char *prev, const char *new, gpointer user_data)
{
	//BluetoothClient *client = BLUETOOTH_CLIENT(user_data);

	if (!g_ascii_strcasecmp(name, "org.bluez") && *new == '\0') {
		/* FIXME disable adapters */
	}
}

static void setup_dbus(BluetoothClient *client)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	DBusGProxy *object;
	GError *error = NULL;

	priv->conn = dbus_g_bus_get(DBUS_BUS_SYSTEM, &error);
	if (error != NULL) {
		g_printerr("Connecting to system bus failed: %s\n",
							error->message);
		g_error_free(error);
		return;
	}

	object = dbus_g_proxy_new_for_name(priv->conn, DBUS_SERVICE_DBUS,
					DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS);

	dbus_g_proxy_add_signal(object, "NameOwnerChanged",
					G_TYPE_STRING, G_TYPE_STRING,
					G_TYPE_STRING, G_TYPE_INVALID);

	dbus_g_proxy_connect_signal(object, "NameOwnerChanged",
				G_CALLBACK(name_owner_changed), client, NULL);
}

static void bluetooth_client_finalize(GObject *object)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(object);

	/* Disconnect the handlers from setup_manager() */
	g_signal_handlers_disconnect_by_func (priv->manager_object, adapter_added, object);
	g_signal_handlers_disconnect_by_func (priv->manager_object, adapter_removed, object);
	g_signal_handlers_disconnect_by_func (priv->manager_object, default_adapter_changed,
					      object);

	priv->registered = FALSE;
	g_free(priv->default_adapter);
	g_object_unref(G_OBJECT(priv->store));
	g_object_unref (priv->manager_object);
}

static void bluetooth_client_init(BluetoothClient *client)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);

	priv->registered = FALSE;

	priv->store = gtk_tree_store_new(NUM_COLS, G_TYPE_STRING, G_TYPE_BOOLEAN,
			G_TYPE_OBJECT, G_TYPE_STRING, G_TYPE_UINT,
			G_TYPE_INT, G_TYPE_STRING, G_TYPE_UINT,
			G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN);

	setup_dbus(client);

	setup_manager(client);
}

static void bluetooth_client_set_property(GObject *object, guint prop_id,
					const GValue *value, GParamSpec *pspec)
{
	//BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(object);

	switch (prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void bluetooth_client_get_property(GObject *object, guint prop_id,
					GValue *value, GParamSpec *pspec)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(object);

	switch (prop_id) {
	case PROP_DEFAULT_ADAPTER:
		g_value_set_string (value, priv->default_adapter);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void bluetooth_client_class_init(BluetoothClientClass *klass)
{
	GObjectClass *object_class;

	g_type_class_add_private(klass, sizeof(BluetoothClientPrivate));

	G_OBJECT_CLASS(klass)->finalize = bluetooth_client_finalize;

	G_OBJECT_CLASS(klass)->set_property = bluetooth_client_set_property;
	G_OBJECT_CLASS(klass)->get_property = bluetooth_client_get_property;

	client_table_signals[DISCOVERY_STARTED] =
		g_signal_new ("discovery-started",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				G_STRUCT_OFFSET (BluetoothClientClass,
							discovery_started),
				NULL, NULL,
				marshal_VOID__STRING_BOOLEAN,
				G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_BOOLEAN);

	client_table_signals[DISCOVERY_COMPLETED] =
		g_signal_new ("discovery-completed",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				G_STRUCT_OFFSET (BluetoothClientClass,
							discovery_completed),
				NULL, NULL,
				marshal_VOID__STRING_BOOLEAN,
				G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_BOOLEAN);

	object_class = (GObjectClass *) klass;

	g_object_class_install_property (object_class, PROP_DEFAULT_ADAPTER,
					 g_param_spec_string ("default-adapter", NULL, NULL,
							      NULL, G_PARAM_READABLE));

	dbus_g_object_register_marshaller(marshal_VOID__STRING_UINT_INT,
				G_TYPE_NONE, G_TYPE_STRING, G_TYPE_UINT,
						G_TYPE_INT, G_TYPE_INVALID);

	dbus_g_object_register_marshaller(marshal_VOID__STRING_STRING,
						G_TYPE_NONE, G_TYPE_STRING,
						G_TYPE_STRING, G_TYPE_INVALID);
}

BluetoothClient *bluetooth_client_new(void)
{
	if (self == NULL) {
		self = BLUETOOTH_CLIENT(g_object_new(BLUETOOTH_TYPE_CLIENT, NULL));
		return self;
	} else {
		return g_object_ref(self);
	}
}

gboolean bluetooth_client_register_passkey_agent(BluetoothClient *client,
		const char *path, const char *address, const void *info)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	DBusGProxy *proxy;

	if (priv->registered == FALSE) {
		dbus_g_object_type_install_info(BLUETOOTH_TYPE_CLIENT, info);

		dbus_g_connection_register_g_object(priv->conn, path, G_OBJECT(client));

		priv->registered = TRUE;
	}

	if (priv->default_adapter == NULL)
		return FALSE;

	proxy = dbus_g_proxy_new_for_name(priv->conn, "org.bluez",
				priv->default_adapter, "org.bluez.Security");

	security_register_passkey_agent(proxy, path, address, NULL);

	g_object_unref(proxy);

	return TRUE;
}

static void create_bonding_reply(DBusGProxy *proxy,
					GError *error, gpointer userdata)
{
	//g_printf("create bonding reply\n");
}

gboolean bluetooth_client_create_bonding(BluetoothClient *client,
					gchar *adapter, const gchar *address)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	if (adapter == NULL)
		adapter = priv->default_adapter;

	if (adapter == NULL)
		return FALSE;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		DBusGProxy *object;
		gchar *path;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &path,
						COLUMN_OBJECT, &object, -1);

		if (g_ascii_strcasecmp(path, adapter) == 0) {
			adapter_create_bonding_async(object, address,
						create_bonding_reply, NULL);
			return TRUE;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return FALSE;
}

static void remove_bonding_reply(DBusGProxy *proxy,
					GError *error, gpointer userdata)
{
	//g_printf("remove bonding reply\n");
}

gboolean bluetooth_client_remove_bonding(BluetoothClient *client,
					gchar *adapter, const gchar *address)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	if (adapter == NULL)
		adapter = priv->default_adapter;

	if (adapter == NULL)
		return FALSE;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		DBusGProxy *object;
		gchar *path;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &path,
						COLUMN_OBJECT, &object, -1);

		if (g_ascii_strcasecmp(path, adapter) == 0) {
			adapter_remove_bonding_async(object, address,
						remove_bonding_reply, NULL);
			return TRUE;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return FALSE;
}

static void set_trusted_reply(DBusGProxy *proxy,
					GError *error, gpointer userdata)
{
	//g_printf("set trusted reply\n");
}

gboolean bluetooth_client_set_trusted(BluetoothClient *client,
					gchar *adapter, const gchar *address)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	if (adapter == NULL)
		adapter = priv->default_adapter;

	if (adapter == NULL)
		return FALSE;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		DBusGProxy *object;
		gchar *path;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &path,
						COLUMN_OBJECT, &object, -1);

		if (g_ascii_strcasecmp(path, adapter) == 0) {
			adapter_set_trusted_async(object, address,
						set_trusted_reply, NULL);
			return TRUE;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return FALSE;
}

static void remove_trust_reply(DBusGProxy *proxy,
					GError *error, gpointer userdata)
{
	//g_printf("remove trust reply\n");
}

gboolean bluetooth_client_remove_trust(BluetoothClient *client,
					gchar *adapter, const gchar *address)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	if (adapter == NULL)
		adapter = priv->default_adapter;

	if (adapter == NULL)
		return FALSE;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		DBusGProxy *object;
		gchar *path;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &path,
						COLUMN_OBJECT, &object, -1);

		if (g_ascii_strcasecmp(path, adapter) == 0) {
			adapter_remove_trust_async(object, address,
						remove_trust_reply, NULL);
			return TRUE;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return FALSE;
}

static void disconnect_remote_device_reply(DBusGProxy *proxy,
					GError *error, gpointer userdata)
{
	//g_printf("disconnect remote device reply\n");
}

gboolean bluetooth_client_disconnect(BluetoothClient *client,
					gchar *adapter, const gchar *address)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	if (adapter == NULL)
		adapter = priv->default_adapter;

	if (adapter == NULL)
		return FALSE;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		DBusGProxy *object;
		gchar *path;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &path,
						COLUMN_OBJECT, &object, -1);

		if (g_ascii_strcasecmp(path, adapter) == 0) {
			adapter_disconnect_remote_device_async(object, address,
					disconnect_remote_device_reply, NULL);
			return TRUE;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return FALSE;
}

gboolean bluetooth_client_discover_devices(BluetoothClient *client, gchar *adapter)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	if (adapter == NULL)
		adapter = priv->default_adapter;

	if (adapter == NULL)
		return FALSE;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		DBusGProxy *object;
		gchar *path;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &path,
						COLUMN_OBJECT, &object,
						-1);

		if (g_ascii_strcasecmp(path, adapter) == 0) {
			dbus_g_proxy_call(object, "DiscoverDevices",
							NULL, G_TYPE_INVALID);
			return TRUE;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return FALSE;
}

gboolean bluetooth_client_cancel_discovery(BluetoothClient *client, gchar *adapter)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeIter iter;
	gboolean cont;

	if (adapter == NULL)
		adapter = priv->default_adapter;

	if (adapter == NULL)
		return FALSE;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		DBusGProxy *object;
		gchar *path;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &path,
						COLUMN_OBJECT, &object, -1);

		if (g_ascii_strcasecmp(path, adapter) == 0) {
			dbus_g_proxy_call(object, "CancelDiscovery",
							NULL, G_TYPE_INVALID);
			return TRUE;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return FALSE;
}

GtkTreeModel *bluetooth_client_get_model(BluetoothClient *client)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);

	g_object_ref(priv->store);

	return GTK_TREE_MODEL(priv->store);
}

GtkTreeModel *bluetooth_client_get_model_simple(BluetoothClient *client)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeModel *model;
	GtkTreeIter iter;
	gboolean cont;

	model = gtk_tree_model_filter_new(GTK_TREE_MODEL(priv->store), NULL);

	gtk_tree_model_filter_set_visible_column(GTK_TREE_MODEL_FILTER(model),
								COLUMN_ACTIVE);

	if (gtk_tree_model_iter_n_children(model, NULL) != 1)
		return model;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		gboolean active;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_ACTIVE, &active, -1);

		if (active == TRUE) {
			GtkTreePath *path;

			path = gtk_tree_model_get_path(GTK_TREE_MODEL(priv->store), &iter);

			g_object_unref(model);

			model = gtk_tree_model_filter_new(GTK_TREE_MODEL(priv->store), path);

			gtk_tree_path_free(path);

			break;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return model;
}

static gboolean adapter_list_filter(GtkTreeModel *model,
					GtkTreeIter *iter, gpointer data)
{
	gchar *path;
	gboolean active;

	gtk_tree_model_get(model, iter, COLUMN_PATH, &path,
					COLUMN_ACTIVE, &active, -1);

	if (path == NULL)
		return FALSE;

	g_free(path);

	return active;
}

GtkTreeModel *bluetooth_client_get_model_adapter_list(BluetoothClient *client)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeModel *model;

	model = gtk_tree_model_filter_new(GTK_TREE_MODEL(priv->store), NULL);

	gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(model),
					adapter_list_filter, NULL, NULL);

	return model;
}

static gboolean device_active_filter(GtkTreeModel *model,
					GtkTreeIter *iter, gpointer data)
{
	gboolean active;

	gtk_tree_model_get(model, iter, COLUMN_ACTIVE, &active, -1);

	return active;
}

GtkTreeModel *bluetooth_client_get_model_for_adapter(BluetoothClient *client,
								gchar *adapter)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeModel *model;
	GtkTreeIter iter;
	gboolean cont;

	if (adapter == NULL)
		adapter = priv->default_adapter;

	if (adapter == NULL)
		return NULL;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		gchar *path;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &path, -1);

		if (g_ascii_strcasecmp(path, adapter) == 0) {
			GtkTreePath *path;

			path = gtk_tree_model_get_path(GTK_TREE_MODEL(priv->store), &iter);

			model = gtk_tree_model_filter_new(GTK_TREE_MODEL(priv->store), path);

			gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(model),
							device_active_filter, NULL, NULL);

			gtk_tree_path_free(path);

			return model;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return NULL;
}

GtkTreeModel *bluetooth_client_get_model_with_filter(BluetoothClient *client,
			gchar *adapter, GtkTreeModelFilterVisibleFunc func,
								gpointer data)
{
	BluetoothClientPrivate *priv = BLUETOOTH_CLIENT_GET_PRIVATE(client);
	GtkTreeModel *model;
	GtkTreeIter iter;
	gboolean cont;

	if (adapter == NULL)
		adapter = priv->default_adapter;

	if (adapter == NULL)
		return NULL;

	cont = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(priv->store), &iter);

	while (cont == TRUE) {
		gchar *path;

		gtk_tree_model_get(GTK_TREE_MODEL(priv->store), &iter,
						COLUMN_PATH, &path, -1);

		if (g_ascii_strcasecmp(path, adapter) == 0) {
			GtkTreePath *path;

			path = gtk_tree_model_get_path(GTK_TREE_MODEL(priv->store), &iter);

			model = gtk_tree_model_filter_new(GTK_TREE_MODEL(priv->store), path);

			if (func != NULL)
				gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(model),
								       func, data, NULL);

			gtk_tree_path_free(path);

			return model;
		}

		cont = gtk_tree_model_iter_next(GTK_TREE_MODEL(priv->store), &iter);
	}

	return NULL;
}

static gboolean device_bonded_filter(GtkTreeModel *model,
					GtkTreeIter *iter, gpointer data)
{
	gboolean bonded, trusted, connected;

	gtk_tree_model_get(model, iter, COLUMN_BONDED, &bonded,
					COLUMN_TRUSTED, &trusted,
					COLUMN_CONNECTED, &connected, -1);

	return (bonded == TRUE || trusted == TRUE ||
			connected == TRUE) ? TRUE : FALSE;
}

GtkTreeModel *bluetooth_client_get_model_bonded_list(BluetoothClient *client,
								gchar *adapter)
{
	return bluetooth_client_get_model_with_filter(client, adapter,
						device_bonded_filter, NULL);
}

/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file serviceprovider_parser.c
 * @brief database xml parser
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * Implements the Service Provider Database XML parser.
 */

#include "serviceprovider_parser.h"
#include "mbca_serviceprovider_private.h"

#include <libxml/xmlreader.h>
#include <string.h>			/* memset */

/**
 * @brief check root element and version
 *
 * Checks documents root element and version attribute. Correct element is
 * <serviceproviders format="1.0">. reader should be positioned at the begining
 * of the document prior to this call.
 *
 * @param reader a valid xmlTextReaderPtr
 *
 * @returns ' 0' on success
 * @returns '-1' error
 */
static int
check_root (xmlTextReaderPtr reader)
{
	const xmlChar* name;
	xmlChar* value;
	int ret;

	/* get root element */
	for (ret = xmlTextReaderRead (reader);
		ret == 1;
		ret = xmlTextReaderRead (reader))
	{
		if (xmlTextReaderNodeType (reader) != XML_READER_TYPE_ELEMENT)
		{
			/* not interested */
			continue;
		}

		/* make sure it's ours */
		name = xmlTextReaderConstName (reader);
		if (xmlStrcmp (name, BAD_CAST "serviceproviders") != 0)
		{
			g_critical ("this is not a service provider database. "
					  "expected <serviceprovider>, got <%s>",
					  name);
			ret = -1;
		}
		
		break;		
	}
	if (ret == 0)
	{
		g_critical ("document has no root element");
		ret = -1;  /* return in next if */
	}
	if (ret == -1)
	{
		return ret;
	}
	
	/* check format number */
	value = xmlTextReaderGetAttribute (reader, BAD_CAST "format");
	if (xmlStrcmp (value, BAD_CAST "1.0") != 0)
	{
		g_critical ("database format is not supported. "
				  "expected \"1.0\", got \"%s\"",
				  value);
		ret = -1;
	}
	free (value);
	
	return 0;
}

/**
 * @brief parse provider subtree
 *
 * Parses subtree inside <provider> </provider> block. Reader should be
 * positioned to a <provider> element prior to this call. Call returns when end
 * element </provider> is encountered. struct pointed by provider will be 
 * initially cleared with zeroes.
 *
 * @param reader a valid xmlTextReaderPtr
 * @param provider 
 *
 * @returns ' 0' on success
 * @returns '-1' on error
 */
static int
parse_provider (xmlTextReaderPtr reader, MBCAServiceProvider* provider)
{
	int ret;
	
	memset (provider, 0, sizeof (MBCAServiceProvider));
	
	for (ret = xmlTextReaderRead (reader);
		ret == 1;
		ret = xmlTextReaderRead (reader))
	{
		const xmlChar* name;
		const xmlChar* value;
		int type;
		
		name = xmlTextReaderConstName (reader);
		type = xmlTextReaderNodeType (reader);
		
		if (type == XML_READER_TYPE_END_ELEMENT)
		{
			if (xmlStrcmp (name, BAD_CAST "provider") == 0)
			{
				/* end of provider, ret = 1 */
				break;
			}
			continue;
		}
		else if (type != XML_READER_TYPE_ELEMENT)
		{
			/* not interested. */
			continue;
		}
		
		/* found an element */
		if (xmlStrcmp (name, BAD_CAST "name") == 0)
		{
			MBCAServiceProviderName* namestruct;
			const xmlChar* xmllang;

			xmlTextReaderRead (reader); /* get to text */
			xmllang = xmlTextReaderConstXmlLang (reader);
			value = xmlTextReaderConstValue (reader);
			
			namestruct = g_malloc (sizeof (MBCAServiceProviderName));
			namestruct->lang = (gchar*)xmlStrdup (xmllang);
			namestruct->name = (gchar*)xmlStrdup (value);
			
			provider->names = g_slist_prepend (provider->names, namestruct);
			continue;
		}
		else if (xmlStrcmp (name, BAD_CAST "gsm") == 0)
		{
			provider->type = MBCA_NETWORK_GSM;
			
			/* find apn element */
			for (ret = xmlTextReaderRead (reader);
				ret == 1;
				ret = xmlTextReaderRead (reader))
			{
				if (xmlTextReaderNodeType (reader) != 
				    XML_READER_TYPE_ELEMENT)
				{
					/* not interested */
					continue;
				}
				
				/* found an element */
				name = xmlTextReaderConstName (reader);
				if (xmlStrcmp (name , BAD_CAST "apn") != 0)
				{
					g_critical ("document is invalid."
							  "expected <apn>, got <%s>",
							  name);
					return -1;
				}
				
				/* get to text */
				ret = xmlTextReaderRead (reader);
				if (ret != 1)
				{
					g_critical ("document is invalid");
					return -1;
				}
				value = xmlTextReaderConstValue (reader);
				provider->gsm.apn = (gchar*)xmlStrdup (value);
				break;
			}
			continue;
		}
		else if (xmlStrcmp (name, BAD_CAST "cdma") == 0)
		{
			provider->type = MBCA_NETWORK_CDMA;
			continue;
		}
		else if (xmlStrcmp (name, BAD_CAST "username") == 0)
		{
			xmlTextReaderRead (reader); /* get to text */
			value = xmlTextReaderConstValue (reader);
			provider->username = (gchar*)xmlStrdup (value);
			continue;
		}
		else if (xmlStrcmp (name, BAD_CAST "password") == 0)
		{
			xmlTextReaderRead (reader); /* get to text */
			value = xmlTextReaderConstValue (reader);
			provider->password = (gchar*)xmlStrdup (value);
			continue;
		}
		else if (xmlStrcmp (name, BAD_CAST "dns") == 0)
		{
			xmlTextReaderRead (reader); /* get to text */
			value = xmlTextReaderConstValue (reader);
			
			if (!provider->dns1)
			{
				provider->dns1 = (gchar*)xmlStrdup (value);
			}
			else if (!provider->dns2)
			{
				provider->dns2 = (gchar*)xmlStrdup (value);
			}
			else
			{
				/* skip additioanl dns elements silently */
			}
			continue;
		}
		else if (xmlStrcmp (name, BAD_CAST "gateway") == 0)
		{
			xmlTextReaderRead (reader); /* get to text */
			value = xmlTextReaderConstValue (reader);
			provider->gateway = (gchar*)xmlStrdup (value);
			continue;
		}
		else
		{
			g_critical ("invalid document, got \"%s\"", name);
			return -1;
		}
	}
	if (ret != 1)
	{
		g_critical ("invalid document.");
		return -1;
	}
	
	provider->names = g_slist_reverse (provider->names);
	return 0;
}

static void
free_providers (GSList* providers)
{
	GSList* node = providers;
	while (node)
	{
		MBCAServiceProvider* provider = node->data;
		
		free_serviceprovider (provider);
		node = node->next;
	}
	g_slist_free (providers);
}

static void
free_country (ServiceProviderCountry* country)
{
	/*
	 * STRINGS ARE ALLOCATED BY xmlStrdup SO THEY MUST BE FREED USING free()
	 */
	
	free_providers (country->providers);
	free (country->code);
	g_free (country);
}

static void
free_countries (GSList* countries)
{
	GSList* node = countries;
	
	while (node)
	{
		ServiceProviderCountry* country = node->data;
		
		free_country (country);
		node = node->next;
	}
	g_slist_free (countries);
}

/**
 * @brief parse country subtree
 *
 * Parses subtree inside <countryr> </country> block. Reader should be
 * positioned to a <country> element prior to this call. Call returns when end
 * element </country> is encountered.
 *
 * @param reader a valid xmlTextReaderPtr
 *
 * @returns ' 0' on success
 * @returns '-1' on error
 */
static int
parse_country(xmlTextReaderPtr reader, GSList** providers)
{
	int ret;

	/* find next country element */
	for (ret = xmlTextReaderRead (reader);
		ret == 1;
		ret = xmlTextReaderRead (reader))
	{
		MBCAServiceProvider* provider;
		const xmlChar* name;
		int type;
		
		type = xmlTextReaderNodeType (reader);
		if (type == XML_READER_TYPE_END_ELEMENT)
		{
			if (xmlStrcmp (xmlTextReaderConstName (reader),
						BAD_CAST "country") == 0)
			{
				break;
			}
			continue;
		}
		if (type != XML_READER_TYPE_ELEMENT)
		{
			/* not interested */
			continue;
		}
		
		/* found an element */
		name = xmlTextReaderConstName (reader);
		if (xmlStrcmp (name, BAD_CAST "provider") != 0)
		{
			g_critical ("document is invalid. expected <provider>, got <%s>",
					  name);
			ret = -1;
			break;
		}			
		
		/* found provider
		 * parse data
		 */
		provider = g_malloc (sizeof (MBCAServiceProvider));
		ret = parse_provider (reader, provider);
		if (ret)
		{
			free_serviceprovider (provider);
			ret = -1;
			break;
		}
		*providers = g_slist_prepend (*providers, provider);
		ret = 1;
	}
	if (ret != 1)
	{
		free_providers (*providers);
		*providers = NULL;
		return -1;
	}
	
	*providers = g_slist_reverse (*providers);
	return 0;
}

/**
 * @brief parse database
 *
 * Parses the database XML.
 *
 * @param reader a valid xmlTextReaderPtr
 * @param countries pointer to a singly-linked list where ServiceProviderCountry
 *                  structs are stored.
 *
 * @returns ' 0' on success
 * @returns '-1' on error
 */
static gint
parse_xml (xmlTextReaderPtr reader, GSList** countries)
{		
	int ret;
	
	ret = check_root (reader);
	if (ret)
	{
		return -1;
	}

	/* find country elements */
	for (ret = xmlTextReaderRead (reader);
		ret == 1;
		ret = xmlTextReaderRead (reader))
	{
		ServiceProviderCountry* country;
		GSList* providers;	
		
		const xmlChar* name;
		xmlChar* code;
		int type;
		
		type = xmlTextReaderNodeType (reader);
		if (type == XML_READER_TYPE_END_ELEMENT)
		{
			if (xmlStrcmp (xmlTextReaderConstName (reader),
						BAD_CAST "serviceproviders") == 0)
			{
				/* done */
				break;
			}
			continue;
		}
		if (type != XML_READER_TYPE_ELEMENT)
		{
			/* not interested.. */
			 continue;
		}
		
		/* found an element */
		name = xmlTextReaderConstName (reader);
		if (xmlStrcmp (name, BAD_CAST "country") != 0)
		{
			g_critical ("document is invalid. expected <country>, got <%s>",
					  name);
			ret = -1;
			break;
		}

		/* found a country.
		 * get code..
		 */
		code = xmlTextReaderGetAttribute (reader,
								    BAD_CAST "code");
		if (!code)
		{
			g_critical ("document is invalid. <country> has no 'code' "
					  "attribute");
			ret = -1;
			break;
		}

		/* ..and dig in
		 */
		providers = NULL;
		ret = parse_country (reader, &providers);
		if (ret)
		{
			free (code);
			ret =  -1;
			break;
		}

		country = g_malloc (sizeof (ServiceProviderCountry));
		country->code = (gchar*)code; 
		country->providers = providers;
		*countries = g_slist_prepend (*countries, country);
		
		ret = 1;
	}
	if (ret != 1)
	{
		g_print ("FOO\n");
		free_countries (*countries);
		*countries = NULL;
		return -1;
	}
	
	*countries = g_slist_reverse (*countries);
	return 0;
}

gint
parse_service_provider_xml (const gchar* filename, GSList** database)
{
	xmlTextReaderPtr reader;
	GSList* countries;
	int ret;
	
	LIBXML_TEST_VERSION
		
	reader = xmlReaderForFile (filename, NULL, 0);
	if (!reader)
	{
		g_critical ("could not create xmlTextReader for file: %s", filename);
		return -1;
	}	
	
	countries = NULL;
	ret = parse_xml (reader, &countries);

	xmlTextReaderClose (reader);
	xmlFreeTextReader (reader);
	if (ret)
	{
		g_critical ("XML parser failed..");
		ret = -2;
	}
	else
	{
		*database = countries;
		ret = 0;
	}
	
	xmlCleanupParser ();
	xmlMemoryDump ();
	return ret;
}

void
free_service_provider_database (GSList** database)
{
	free_countries (*database);
}

/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file method_page.c
 * @brief Connection Method page functionality
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 */

#include "callbacks.h"

static void
set_hal_device (gchar* device_type,
			 gchar* primary,
			 gchar* secondary,
			 gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkLabel* secondary_label;
	GtkLabel* primary_label;
	GtkWidget* alignment;
	GtkWidget* hal_page;
	GtkWidget* page;
	
	priv->conf->type = MBCA_DEVICE_HAL;
	priv->hal_device_type = device_type;
	
	hal_page = gtk_assistant_get_nth_page (priv->assistant, PAGE_HAL);
	
	gtk_assistant_set_page_title (priv->assistant,
							hal_page,
							device_type);
	
	primary_label = GTK_LABEL (gtk_builder_get_object (priv->builder,
											 "hal_page_primary_"
											 "info_label"));
	secondary_label = GTK_LABEL (gtk_builder_get_object (priv->builder,
											   "hal_page_secondary_"
											   "info_label"));
	alignment = GTK_WIDGET (gtk_builder_get_object (priv->builder,
										   "hal_page_secondary_"
										   "info_alignment"));
	g_return_if_fail (primary_label);
	g_return_if_fail (secondary_label);
	g_return_if_fail (alignment);
	
	gtk_label_set_markup (primary_label, primary);
	if (!secondary)
	{
		/* hide the secondary information */
		gtk_widget_hide (alignment);
	}
	else
	{
		/* user might go backwards from page that does not havesecondary info
		 * to a page which has.
		 */
		gtk_widget_show (alignment);
		gtk_label_set_markup (secondary_label, secondary);
	}
		
	page = gtk_assistant_get_nth_page (priv->assistant, PAGE_METHOD);
	gtk_assistant_set_page_complete (priv->assistant, page, TRUE);	
}

void
mbca_method_page_prepare_cb (GtkAssistant* gtkassistant __attribute__ ((unused)),
					    GtkWidget* page __attribute__ ((unused)),
					    gpointer user_data __attribute__ ((unused)))
{
	
}

void
mbca_bluetooth_radiobutton_clicked_cb (GtkButton* button __attribute__ ((unused)),
							    gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkWidget* page;

	priv->conf->type = MBCA_DEVICE_BLUETOOTH;
	
	page = gtk_assistant_get_nth_page (priv->assistant, PAGE_METHOD);
	gtk_assistant_set_page_complete (priv->assistant, page, TRUE);
	
	/* lookahead :P
	 * If user decides to choose some other page mbca_assistant_prepare_cb ()
	 * anyway sets this to FALSE when page is changed.
	 */
	priv->btrescan_enabled = TRUE;
}

void
mbca_builtin_radiobutton_clicked_cb (GtkButton* button __attribute__ ((unused)),
							  gpointer user_data)
{
	set_hal_device (_("Built-in"),
				 _("Select the built-in device from the list"),
				 _("<small>Make sure the device is not disabled by a "
				   "manual switch or button on the side of the computer"
				   ".</small>"),
				 user_data);	
}

void
mbca_pcmcia_radiobutton_clicked_cb (GtkButton* button __attribute__ ((unused)),
							 gpointer user_data)
{
	set_hal_device (_("PC Card, PCMCIA, or ExpressCard"),
				 _("Select the correct device from the list"),
				 _("<small>Make sure the SIM card is in place</small>"),
				 user_data);	
}

void
mbca_serialcable_radiobutton_clicked_cb (GtkButton* button __attribute__ ((unused)),
								 gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkWidget* page;

	priv->conf->type = MBCA_DEVICE_SERIAL;
	
	page = gtk_assistant_get_nth_page (priv->assistant, PAGE_METHOD);
	gtk_assistant_set_page_complete (priv->assistant, page, TRUE);
}

void
mbca_usb_radiobutton_clicked_cb (GtkButton* button __attribute__ ((unused)),
						   gpointer user_data)
{
	set_hal_device (_("USB"),
				 _("Select the USB device from the list"),
				 _("<small>The device must be plugged in and must be switched on."
				   " Make sure that the mobile phone has no security lock acti"
				   "ve. Make sure the SIM card is in place</small>"),
				 user_data);	
}

/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file common.h
 * @brief defines common bits and pieces
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * Definition of common structures and includes.
 */
 
#ifndef MBCA_COMMON_H
#define MBCA_COMMON_H

#include "../config.h"

#include "mbca_serviceprovider_private.h"
#include "serviceprovider_parser.h"
#include "mbca_assistant.h"

#include "bluez-gnome/bluetooth-device-selection.h"
#include "bluez-gnome/client.h"
 
#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>
#include <hal/libhal.h>

#define GWEATHER_I_KNOW_THIS_IS_UNSTABLE
#include <libgweather/gweather-xml.h>

#define RESOURCE_DIR PACKAGE_DATA_DIR "/" PACKAGE
#define MBCA_UI_DEFINITION RESOURCE_DIR "/mbca.glade"

G_BEGIN_DECLS

/**
 * @brief page numbers
 *
 * Page numbers for assistants pages.
 * 
 * @note If pages are added to the UI definition this enum must be updated to
 *	    correspond with the new page layout.
 */
enum AssistantPages
{
	PAGE_INTRO = 0,
	PAGE_METHOD,
	PAGE_BLUETOOTH,
	PAGE_SERIAL,
	PAGE_HAL,
	PAGE_PROVIDER,
	PAGE_SUMMARY
};

/**
 * @brief serial liststore columns
 *
 * columns of serial device list on serial page
 */
enum SerialListStoreColumns
{
	SERIAL_SUBSYSTEM_COLUMN, /**< subsystem reported by HAL	*/
	SERIAL_PORT_COLUMN,		/**< device path				*/
	SERIAL_N_COLUMNS		/**< total number of columns		*/
};

/**
 * @brief serial baud liststore columns
 *
 * columns of baud rate list on serial page
 */
enum SerialBaudListStoreColumns
{
	SERIALBAUD_RATE_COLUMN,  /**< BAUD rate				*/
	SERIALBAUD_N_COLUMNS	/**< total number of columns  */
};

/**
 * @brief hal device liststore columns
 *
 * columns of device list on HAL page
 */
enum HALDeviceListStoreColumns
{
	HAL_PRODUCT_COLUMN, /**< device product name */
	HAL_VENDOR_COLUMN,  /**< device vendor name */
	HAL_UDI_COLUMN,	/**< HAL udi of the device */
	HAL_N_COLUMNS		/**< total number of columns  */
};

/**
 * @brief country liststore columns
 *
 * columns of country list on provider page
 */
enum CountryListStoreColumns
{
	COUNTRY_CODE_COLUMN,		/**< ISO 3166-1 alpha-2 code of a country  */
	COUNTRY_NAME_COLUMN,		/**< localized name of a country		   */
	COUNTRY_PROVIDERS_COLUMN,	/**< GSList of providers of a country	   */
	COUNTRY_SORTED_COLUMN,		/**< helper for automatic sorting		   */
	COUNTRY_N_COLUMNS			/**< total number of columns			   */
};

/**
 * @brief providers liststore columns
 *
 * columns of provider list on provider page
 */
enum ProvidersListStoreColumns
{
	PROVIDER_NAME_COLUMN,    /**< name of the service provider		*/
	PROVIDER_DATA_COLUMN,    /**< pointer to ServiceProvider struct  */
	PROVIDER_N_COLUMNS		/**< total number of columns			*/
};

/**
 * @brief class private members
 *
 * Private members of the assistant class.
 * All databases and models are shared between instances.
 */
struct _MBCAAssistantClassPrivate
{

};

/**
 * @brief instance private members
 *
 * Private members of a instance of the assistant.
 */
struct _MBCAAssistantPrivate
{
	gboolean dispose_has_run;

	MBCAAssistantState state;
	
	/* Generic */
	GtkBuilder* builder;	/**< GtkBuilder instance containing the GUI that
						 *   is built from UI Definition         */
	GtkAssistant* assistant; /**< commodity pointer to mbca_assistant */

	MBCAConfiguration* conf; /**< configuration that is filled during steps */
	
	gchar* nice_device_name; /**< humanly pleasant name for device          */
	gchar* hal_device_type;  /**< device type user has selected for HAL device
						 */
	gchar* provider_name;    /**< name of the selected provider for summary */
	gchar* country_name;	/**< name of the selected country for summary  */

	
	gboolean device_is_preset; /**< set TRUE if device and address are preset
						   */
	gboolean done;			  /**< TRUE if apply() callback was reached */


	/* Bluetooth page */
	GtkWidget* btselector; /**< Bluetooth device selector widget */

	GThread* btrescan_thread; /**< rescans for new BT devices */

	volatile gboolean btrescan_enabled; /**< if TRUE rescan is enabled     */
	volatile gboolean exit_btrescan;    /**< if TRUE btrescan thread exits */


	/* Serial page */
	gboolean serial_device_set; /**< TRUE if conf.device is set */
	gboolean serial_baud_set;   /**< TRUE if conf.baud is set   */


	/* HAL page*/
	gboolean hal_page_initialized; /**< TRUE, if hal_page is initialized */


	/* Service provider page */
	gboolean country_list_sorted; /**< TRUE is country list is initially
							 *   sorted.				    */ 

	gboolean resources_attached;  /**< part of the workaround... */
							/** @todo fixme */


	/* Resources */
	DBusConnection* halconn; /**< connection to HAL */
	LibHalContext* halctx;   /**< HAL context for halconn */

	GtkListStore* serial_store;
	GtkListStore* baud_store;
	GtkListStore* hal_store;		   /**< constains available HAL devices  */	


	/* Provider page resource loading thread
	 *  - these members should be accessed with great care to avoid any nasty
	 *    race or other concurrency issues
	 */
	GThread* provider_thread; /**< thread for loading provider page data
						  *   in the background */

	GtkTreeModel* gweather_database; /**< database containing country
							    *   information from libgweather */
	
	GtkListStore* country_store;	   /**< contains available countries */

	gchar* country_code; /**< ISO 3166-1 alpha-2 code of systems country */
	
	volatile gboolean abort_provider_thread; /**< if TRUE interrupts loading
									  */
	volatile gboolean provider_thread_ready; /**< TRUE if loading is done   */
	
	GMutex* instances_mutex;	  /**< mutex for waiting_instances */
	GSList* waiting_instances; /**< instances waiting for resources, see
						   *   comments at the end of assistant_init()
						   */ 
						   
	GMutex* trigger_mutex; /**< this is only a workaround of a thread problem 
					    */
	
	GSList* database;			   /**< service provider database    */
};


/**
 * @brief converts page names to corresponding numbers.
 *
 * Numbers are easy to process and they are used with many GtkAssistant
 * methods.
 *
 * @param name of a page
 *
 * @returns page number
 */
enum AssistantPages
mbca_page_name_to_number (const gchar* name);


G_END_DECLS

#endif /* MBCA_COMMON_H */

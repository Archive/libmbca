/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file common.c
 * @brief implements common bits and pieces
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * Implementation of common structures and includes.
 */

#include "common.h"

enum AssistantPages
mbca_page_name_to_number (const gchar* name)
{
	if (g_utf8_collate (name, "intro_page") == 0)
	{
		return PAGE_INTRO;
	}
	else if (g_utf8_collate (name, "method_page") == 0)
	{
		return PAGE_METHOD;
	}
	else if (g_utf8_collate (name, "bluetooth_page") == 0)
	{
		return PAGE_BLUETOOTH;
	}
	else if (g_utf8_collate (name, "serial_page") == 0)
	{
		return PAGE_SERIAL;
	}
	else if (g_utf8_collate (name, "hal_page") == 0)
	{
		return PAGE_HAL;
	}
	else if (g_utf8_collate (name, "provider_page") == 0)
	{
		return PAGE_PROVIDER;
	}
	else if (g_utf8_collate (name, "summary_page") == 0)
	{
		return PAGE_SUMMARY;
	}
	else
	{
		g_warn_if_reached ();
	}

	return PAGE_INTRO;
}

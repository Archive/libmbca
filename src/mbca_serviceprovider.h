/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file mbca_serviceprovider.h
 * @brief public API
 *
 * This file contains structs and enums that describe service providers.
 * The file forms together with mbca_assistant.h public API of libmbca.
 *
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 */
 

#ifndef MBCA_SERVICEPROVIDER_H
#define MBCA_SERVICEPROVIDER_H

#include <glib.h>

G_BEGIN_DECLS

/**
 * @brief cellural network type
 *
 * Cellural network a provider is using. Network type helps to choose the right
 * dial sequence as data modems are standardised.
 */
enum MBCANetwork
{
	MBCA_NETWORK_GSM,      /**< GSM network, means GPRS, UMTS, etc     */
	MBCA_NETWORK_CDMA      /**< CDMA network					  */
};

/**
 * @brief GSM information
 *
 * GSM network specific information. 
 */
struct _mbcaGSMInfo
{
	gchar* apn;    /**< Access Point Name */
};
/** standard typedef */
typedef struct _mbcaGSMInfo mbcaGSMInfo;

/**
 * @brief service provider name
 *
 * Mobile Broadband Service Provider Database supports multiple names in
 * different languages for a service provider and this struct contains name of
 * the service provider in some language. 
 */
struct _MBCAServiceProviderName
{
	gchar* lang;  /**< language, contains xml:lang attribute as defined in
				*   database                                             */
	gchar* name;  /**< name of the service provider                         */
};
/** standard typedef */
typedef struct _MBCAServiceProviderName MBCAServiceProviderName;


 /**
 * @brief service provider data
 *
 * Contains the data of one service provider. In general NULL means
 * "not needed" or "undefined". Fields correspond to Mobile Broadband Service
 * Provider Database format.
 *
 * @see http://live.gnome.org/NetworkManager/MobileBroadband/ServiceProviders
 */
struct _MBCAServiceProvider
{
	GSList* names;		   /**< List of _MBCAServiceProviderName structs.
					    *   Contains names as defined in the database.  */
	enum MBCANetwork type; /**< cellural network the provider is using	  */
	union				
	{
		mbcaGSMInfo gsm;  /**< contains GSM network specific settings if 
					    *   _MBCAServiceProvider::type is MBCA_NETWORK_GSM 
					    *									  */
	};				   /**< union containing network specific settings.
					    *   _MBCAServiceProvider::type indicates valid
					    *   member */

	gchar* username;	   /**< if NULL, no authentication required         */
	gchar* password;	   /**< if NULL, no password required.			  */

	gchar* dns1;		   /**< primary DNS, NULL if automatically acquired */
	gchar* dns2;		   /**< secondary DNS, NULL if none.			  */
	gchar* gateway;	   /**< gateway to use, NULL if none			  */
};
/** standard typedef */
typedef struct _MBCAServiceProvider MBCAServiceProvider;

G_END_DECLS

#endif /* MBCA_SERVICEPROVIDER_H */
 

/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "mbca_serviceprovider.h"

#include <stdlib.h>
#include <string.h>

#define _dupstr(d, s)    \
if (s)				\
{					\
	d = strdup (s);	\
}					\

MBCAServiceProvider*
duplicate_serviceprovider (MBCAServiceProvider* provider)
{
	MBCAServiceProvider* newprovider;
	GSList* node;

	if (!provider)
	{
		return NULL;
	}
	newprovider = g_malloc0 (sizeof (MBCAServiceProvider));

	node = provider->names;		
	while (node)
	{
		MBCAServiceProviderName* newname;
		MBCAServiceProviderName* name = node->data;

		newname = g_malloc0 (sizeof (MBCAServiceProviderName));
		_dupstr(newname->lang, name->lang);
		_dupstr(newname->name, name->name);
		newprovider->names = g_slist_prepend (newprovider->names, newname);
	
		node = node->next;
	}
	newprovider->names = g_slist_reverse (newprovider->names);
	
	newprovider->type = provider->type;
	if (provider->type == MBCA_NETWORK_GSM)
	{
		_dupstr(newprovider->gsm.apn, provider->gsm.apn);
	}
	
	_dupstr(newprovider->username, provider->username);
	_dupstr(newprovider->password, provider->password);
	_dupstr(newprovider->dns1, provider->dns1);
	_dupstr(newprovider->dns2, provider->dns2);
	_dupstr(newprovider->gateway, provider->gateway);
	
	return newprovider;
}

/** 
 * frees also the MBCAServiceProvider struct;
 */
void
free_serviceprovider (MBCAServiceProvider* provider)
{
	GSList* node;
		
	if (!provider)
	{
		return;
	}
	
	/*
	 * STRINGS ARE ALLOCATED BY xmlStrdup SO THEY MUST BE FREED USING free()
	 */

	node = provider->names;
	while (node)
	{
		MBCAServiceProviderName* name = node->data;

		free (name->lang);
		free (name->name);
		g_free (name);
		node = node->next;
	}
	g_slist_free (provider->names);
	
	if (provider->type == MBCA_NETWORK_GSM)
	{
		free (provider->gsm.apn);
	}
	
	free (provider->username);
	free (provider->password);
	free (provider->dns1);
	free (provider->dns2);
	free (provider->gateway);
	g_free (provider);	
}

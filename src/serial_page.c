/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file serial_page.c
 * @brief Connection Method page functionality
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 */

#include "callbacks.h"

void
mbca_serial_page_prepare_cb (GtkAssistant* gtkassistant __attribute__ ((unused)),
					    GtkWidget* page __attribute__ ((unused)),
					    gpointer user_data __attribute__ ((unused)))
{
	
}

static void
check_complete (MBCAAssistantPrivate* priv)
{
	GtkWidget* page = gtk_assistant_get_nth_page (priv->assistant,
										 PAGE_SERIAL);
	gboolean is_complete = (priv->serial_device_set && priv->serial_baud_set);
	gtk_assistant_set_page_complete (priv->assistant, page, is_complete);
	
	g_free (priv->nice_device_name);
	priv->nice_device_name = g_strdup (priv->conf->device);
}

void
mbca_serial_manual_checkbutton_toggled_cb (GtkToggleButton* togglebutton,
								   gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkWidget* alignment; /* alignment containing a treeview of serial ports */
	GtkWidget* hbox;	  /* hbox containing manual path entry */
	
	alignment = GTK_WIDGET (gtk_builder_get_object (priv->builder,
										   "serial_list_alignment"));
	hbox = GTK_WIDGET (gtk_builder_get_object (priv->builder,
									   "serial_manual_hbox"));
	g_return_if_fail (alignment);
	g_return_if_fail (hbox);
	
	priv->serial_device_set = FALSE;
	priv->serial_baud_set = FALSE;
	
	if (gtk_toggle_button_get_active (togglebutton))
	{
		GObject* serial_entry;
		GObject* baud_entry;
		gtk_widget_set_sensitive (alignment, FALSE);
		gtk_widget_set_sensitive (hbox, TRUE);

		/* check for content */
		serial_entry = gtk_builder_get_object (priv->builder,
									    "serial_manual_entry");
		baud_entry = gtk_builder_get_object (priv->builder,
									  "serial_manual_baud_entry");
		g_return_if_fail (serial_entry);
		g_return_if_fail (baud_entry);
		mbca_serial_manual_entry_changed_cb (GTK_EDITABLE (serial_entry),
									  priv);
		mbca_serial_manual_baud_entry_changed_cb (GTK_EDITABLE (baud_entry),
										  priv);
	}
	else
	{
		GObject* serial_view;
		GObject* baud_view;
		GtkTreeSelection* selection;
		
		gtk_widget_set_sensitive (alignment, TRUE);
		gtk_widget_set_sensitive (hbox, FALSE);

		serial_view = gtk_builder_get_object (priv->builder,
									   "serial_treeview");
		baud_view = gtk_builder_get_object (priv->builder,
									 "serial_baud_treeview");
		g_return_if_fail (serial_view);
		g_return_if_fail (baud_view);
		
		selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (serial_view));
		mbca_serial_treeview_selection_changed_cb (selection, priv);
		selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (baud_view));
		mbca_serial_baud_treeview_selection_changed_cb (selection, priv);
	}
}

void
mbca_serial_manual_entry_changed_cb (GtkEditable* editable,
							  gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	
	gchar* chars;
	chars = gtk_editable_get_chars (editable, 0, -1);
	if (g_utf8_strlen (chars, -1) > 0)
	{
		g_free (priv->conf->device);
		priv->conf->device = chars;
		priv->serial_device_set = TRUE;
	}
	else
	{
		g_free (chars);
		priv->serial_device_set = FALSE;
	}

	check_complete (priv);
}

void
mbca_serial_manual_baud_entry_changed_cb (GtkEditable* editable,
								  gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	
	gchar* chars;
	chars = gtk_editable_get_chars (editable, 0, -1);
	if (g_utf8_strlen (chars, -1) > 0)
	{
		g_free (priv->conf->baud);
		priv->conf->baud = chars;
		priv->serial_baud_set = TRUE;
	}
	else
	{
		g_free (chars);
		priv->serial_baud_set = FALSE;
	}
	
	check_complete (priv);
}

void
mbca_serial_treeview_selection_changed_cb (GtkTreeSelection* selection,
								   gpointer          user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkTreeModel* model;
	GtkTreeIter iter;
	
	if( gtk_tree_selection_get_selected (selection, &model, &iter))
	{
		g_free (priv->conf->device);
		gtk_tree_model_get (model, &iter,
						SERIAL_PORT_COLUMN, &priv->conf->device,
						-1);
		priv->serial_device_set = TRUE;
	}
	else
	{
		priv->serial_device_set = FALSE;
	}
	
	check_complete (priv);
}


void
mbca_serial_baud_treeview_selection_changed_cb (GtkTreeSelection* selection,
									   gpointer          user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkTreeModel* model;
	GtkTreeIter iter;
	
	if( gtk_tree_selection_get_selected (selection, &model, &iter))
	{
		g_free (priv->conf->baud);
		gtk_tree_model_get (model, &iter,
						SERIALBAUD_RATE_COLUMN, &priv->conf->baud,
						-1);
		priv->serial_baud_set = TRUE;
	}
	else
	{
		priv->serial_baud_set = FALSE;
	}
	
	check_complete (priv);	
}

/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "callbacks.h"

void
mbca_summary_name_entry_changed_cb (GtkEditable* editable,
							 gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	gboolean complete;
	GtkWidget* page;
	gchar* chars;
	
	chars = gtk_editable_get_chars (editable, 0, -1);
	
	g_free (priv->conf->name);
	priv->conf->name = chars;
	complete = (g_utf8_strlen (chars, -1) > 0);
	
	page = gtk_assistant_get_nth_page (priv->assistant, PAGE_SUMMARY);
	gtk_assistant_set_page_complete (priv->assistant, page, complete);
}

void
mbca_summary_page_prepare_cb (GtkAssistant* gtkassistant __attribute__ ((unused)),
						GtkWidget* page __attribute__ ((unused)),
						gpointer user_data)
{
	MBCAAssistant* assistant = user_data;
	MBCAAssistantPrivate* priv = assistant->priv;
	
	GtkLabel* method;
	GtkLabel* device;
	GtkLabel* country;
	GtkLabel* provider;
	GtkEntry* name;
	gchar* namestr;
	
	method = GTK_LABEL (gtk_builder_get_object (priv->builder,
									    "summary_method_label"));
	device = GTK_LABEL (gtk_builder_get_object (priv->builder,
									    "summary_device_label"));
	country = GTK_LABEL (gtk_builder_get_object (priv->builder,
										"summary_country_label"));
	provider = GTK_LABEL (gtk_builder_get_object (priv->builder,
										 "summary_provider_label"));
	name = GTK_ENTRY (gtk_builder_get_object (priv->builder,
									  "summary_name_entry"));
	
	switch (priv->conf->type)
	{
		case MBCA_DEVICE_BLUETOOTH:
		{
			gtk_label_set_text (method, _("Bluetooth"));
			break;	
		}
		case MBCA_DEVICE_SERIAL:
		{
			gtk_label_set_text (method, _("Serial cable"));
			break;
		}
		case MBCA_DEVICE_HAL:
		{
			gtk_label_set_text (method, priv->hal_device_type);
			break;
		}
		case MBCA_DEVICE_PSEUDO:
		{
			GObject* caption;
			caption = gtk_builder_get_object (priv->builder,
									    "summary_device_caption_label");
			g_return_if_fail (caption);
			
			/* hide device name */
			gtk_widget_hide (GTK_WIDGET (caption));
			gtk_widget_hide (GTK_WIDGET (device));
			break;
		}		
		default:
		{
			g_return_if_reached ();
		}
	}

	if (priv->device_is_preset)
	{
		/* hide connection method field as it's unknown and unrelevant */
		GObject* caption;

		caption = gtk_builder_get_object (priv->builder,
								    "summary_method_caption_label");
		gtk_widget_hide (GTK_WIDGET (caption));						    
		gtk_widget_hide (GTK_WIDGET (method));
	}	
	
	gtk_label_set_text (device, priv->nice_device_name);
	gtk_label_set_text (country, priv->country_name);
	gtk_label_set_text (provider, priv->provider_name);

	if (priv->conf->type == MBCA_DEVICE_PSEUDO)
	{
		namestr = g_strdup (priv->provider_name);
	}
	else
	{
		namestr = g_strjoin (NULL,
						 priv->nice_device_name,
						 " - ",
						 priv->provider_name,
						 NULL);
	}
	gtk_entry_set_text (name, namestr);
	g_free (priv->conf->name);
	priv->conf->name = namestr;
	
	mbca_summary_name_entry_changed_cb (GTK_EDITABLE (name), priv);	
}

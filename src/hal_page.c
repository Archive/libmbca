/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "callbacks.h"

/*
 * Strings for different the pages are found from method_page.c
 */

void
mbca_hal_page_prepare_cb (GtkAssistant* gtkassistant __attribute__((unused)),
					 GtkWidget* page __attribute__((unused)),
					 gpointer user_data)
{
	MBCAAssistant* assistant = user_data;
	MBCAAssistantPrivate* priv = assistant->priv;
	
	if (!priv->hal_page_initialized)
	{
		/** @todo fixme */
		priv->hal_page_initialized = TRUE;
	}
	
}

void
mbca_hal_treeview_selection_changed_cb (GtkTreeSelection* selection,
							     gpointer          user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkTreeModel* model;
	GtkTreeIter iter;
	GtkWidget* page;
	
	if( gtk_tree_selection_get_selected (selection, &model, &iter))
	{
		g_free (priv->conf->device);
		gtk_tree_model_get (model, &iter,
						HAL_UDI_COLUMN, &priv->conf->device,
						-1);
		g_free (priv->nice_device_name);
		gtk_tree_model_get (model, &iter,
						HAL_PRODUCT_COLUMN, &priv->nice_device_name,
						-1);
		
		page = gtk_assistant_get_nth_page (priv->assistant, PAGE_HAL);
		gtk_assistant_set_page_complete (priv->assistant, page, TRUE);
	}
}




/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file callbacks.h
 * @brief GUI callbacks
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * Defines GUI callbacks. Functions are documented in C-files.
 */
 
#ifndef MBCA_CALLBACKS_H
#define MBCA_CALLBACKS_H

#include "common.h"

G_BEGIN_DECLS

/*******************************************************************************
 * GtkAssistant - main.c *
 *************************/
void
mbca_assistant_apply_cb (GtkAssistant* assistant,
					gpointer user_data);
void
mbca_assistant_cancel_cb (GtkButton* button,
					 gpointer user_data);
void
mbca_assistant_close_cb (GtkButton* button,
					gpointer user_data);
void
mbca_assistant_prepare_cb (GtkAssistant* assistant,
					  GtkWidget* page,
					  gpointer user_data);


/*******************************************************************************
 * Connection method page - method_page.c *
 ******************************************/
void
mbca_method_page_prepare_cb (GtkAssistant* assistant,
					    GtkWidget* page,
					    gpointer user_data);

void
mbca_bluetooth_radiobutton_clicked_cb (GtkButton* button,
							    gpointer user_data);
void
mbca_builtin_radiobutton_clicked_cb (GtkButton* button,
						       gpointer user_data);
void
mbca_pcmcia_radiobutton_clicked_cb (GtkButton* button,
						      gpointer user_data);
void
mbca_serialcable_radiobutton_clicked_cb (GtkButton* button,
								 gpointer user_data);
void
mbca_usb_radiobutton_clicked_cb (GtkButton* button,
						   gpointer user_data);

/*******************************************************************************
 * Bluetooth page - bluetooth_page.c *
 *************************************/
 
void
mbca_bluetooth_page_prepare_cb (GtkAssistant* assistant,
						  GtkWidget* page,
						  gpointer user_data);

void
mbca_bluetooth_selected_device_changed(BluetoothDeviceSelection* selector,
							    gchar* address,
							    gpointer user_data);
				    
void
mbca_bluetooth_manual_checkbutton_toggled_cb (GtkToggleButton* togglebutton,
									 gpointer user_data);

void
mbca_bluetooth_manual_entry_changed_cb (GtkEditable* editable,
								gpointer user_data);
						
gpointer
mbca_bluetooth_rescan_cb (gpointer data);

/*******************************************************************************
 * Serial cable page - serial_page.c *
 *************************************/
void
mbca_serial_page_prepare_cb (GtkAssistant* assistant,
					    GtkWidget* page,
					    gpointer user_data);
void
mbca_serial_manual_checkbutton_toggled_cb (GtkToggleButton* togglebutton,
								   gpointer user_data);

void
mbca_serial_manual_entry_changed_cb (GtkEditable* editable,
							  gpointer user_data);

void
mbca_serial_manual_baud_entry_changed_cb (GtkEditable* editable,
								  gpointer user_data);
void
mbca_serial_treeview_selection_changed_cb (GtkTreeSelection* selection,
								   gpointer          user_data);

void
mbca_serial_baud_treeview_selection_changed_cb (GtkTreeSelection* selection,
									   gpointer          user_data);

/*******************************************************************************
 * HAL page - hal_page.c *
 *************************/
void
mbca_hal_page_prepare_cb (GtkAssistant* assistant,
					 GtkWidget* page,
					 gpointer user_data);

void
mbca_hal_treeview_selection_changed_cb (GtkTreeSelection* selection,
							     gpointer          user_data);

/*******************************************************************************
 * Service provider page - provider_page.c *
 *******************************************/
void
mbca_provider_page_prepare_cb (GtkAssistant* assistant,
					      GtkWidget* page,
					      gpointer user_data);
void
mbca_country_selection_changed_cb (GtkTreeSelection* selection,
							gpointer user_data);
void
mbca_provider_selection_changed_cb (GtkTreeSelection* selection,
							 gpointer user_data);

gboolean
mbca_country_code_qyery_tooltip_cb (GtkWidget* widget,
							 gint x,
							 gint y,
							 gboolean keyboard_mode,
							 GtkTooltip* tooltip,
							 gpointer userdata);
gpointer
mbca_construct_provider_page_cb (gpointer data);

/*******************************************************************************
 * Summary page - summary_page.c *
 *********************************/
void
mbca_summary_page_prepare_cb (GtkAssistant* assistant,
					     GtkWidget* page,
					     gpointer user_data);
void
mbca_summary_name_entry_changed_cb (GtkEditable* editable,
							 gpointer user_data);

G_END_DECLS
#endif /* MBCA_CALLBACKS_H */

/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "callbacks.h"

void
mbca_bluetooth_page_prepare_cb (GtkAssistant* gtkassistant __attribute__((unused)),
						  GtkWidget* page __attribute__((unused)),
						  gpointer user_data __attribute__((unused)))
{
	/* bluetooth rescan is controlled in mbca_assistant_prepare_cb() */
}

void
mbca_bluetooth_selected_device_changed(BluetoothDeviceSelection* selector,
							    gchar* address,
							    gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkWidget* page;
	gchar* name;
	
	
	g_object_get (G_OBJECT (selector), "device-selected-name", &name, NULL);

	priv->nice_device_name = name;
	priv->conf->device = g_strdup (address);
	
	page = gtk_assistant_get_nth_page (priv->assistant, PAGE_BLUETOOTH);
	gtk_assistant_set_page_complete (priv->assistant, page, TRUE);
}

void
mbca_bluetooth_manual_checkbutton_toggled_cb (GtkToggleButton* togglebutton,
									 gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GObject* manual_entry;
	GObject* manual_box;
	GObject* bt_box;
	gboolean set;
	
	manual_box = gtk_builder_get_object (priv->builder, 
								  "bluetooth_manual_hbox");
	bt_box = gtk_builder_get_object (priv->builder,
							   "bluetooth_placeholder_vbox");
	manual_entry = gtk_builder_get_object (priv->builder,
								    "bluetooth_manual_entry");
	g_return_if_fail (manual_box);
	g_return_if_fail (bt_box);
	g_return_if_fail (manual_entry);
	
	set = gtk_toggle_button_get_active (togglebutton);

	gtk_widget_set_sensitive (GTK_WIDGET (manual_box), set);
	gtk_widget_set_sensitive (GTK_WIDGET (bt_box), !set);
	
	if (set)
	{
		mbca_bluetooth_manual_entry_changed_cb (GTK_EDITABLE (manual_entry),
										priv);
	}
	else
	{
		BluetoothDeviceSelection* selector;
		gchar* address;
		
		selector = BLUETOOTH_DEVICE_SELECTION (priv->btselector);
		g_object_get (G_OBJECT (selector),
				    "device-selected",
				    &address,
				    NULL);
		if (!address)
		{
			GtkWidget* page;
			
			page = gtk_assistant_get_nth_page (priv->assistant,
										PAGE_BLUETOOTH);
			gtk_assistant_set_page_complete (priv->assistant, page, FALSE);				
			return;
		}
		mbca_bluetooth_selected_device_changed(selector, address, priv);
		g_free (address);
	}
}

void
mbca_bluetooth_manual_entry_changed_cb (GtkEditable* editable,
								gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	gboolean is_valid;
	GtkWidget* page;
	gchar* chars;
	gint i;
	
	/* validate */
	chars = gtk_editable_get_chars (editable, 0, -1);
	is_valid = TRUE;
	for (i = 0; i < 6; i++)
	{
		gint offset = i*3;
		
		if (!g_ascii_isxdigit (chars[offset]))
		{
			is_valid = FALSE;
			break;
		}
		if (!g_ascii_isxdigit (chars[offset+1]))
		{
			is_valid = FALSE;
			break;
		}
		
		if (i != 5)
		{
			if (chars[offset+2] != ':')
			{
				is_valid = FALSE;
				break;
			}
		}
	}

	page = gtk_assistant_get_nth_page (priv->assistant,
								PAGE_BLUETOOTH);
	gtk_assistant_set_page_complete (priv->assistant, page, is_valid);	
	if (!is_valid)
	{
		return;
	}
	
	g_free (priv->conf->device);
	g_free (priv->nice_device_name);
	priv->conf->device = chars;
	priv->nice_device_name = g_strdup (priv->conf->device);
}

gpointer
mbca_bluetooth_rescan_cb (gpointer data)
{
	MBCAAssistantPrivate* priv = data;
	
	gboolean was_enabled = FALSE;
	gulong slept = 0;
		
	while (!priv->exit_btrescan)
	{
		if (!was_enabled && priv->btrescan_enabled)
		{
			/* scan immediately when enable is set */
			slept = 5000000;
		}
		was_enabled = priv->btrescan_enabled;
		if (slept < 5000000)
		{
			g_usleep (500000);
			slept += 500000;
			continue;
		}
		slept = 0;		
		
		if (priv->btrescan_enabled)
		{
			BluetoothDeviceSelection* selector;
			selector = BLUETOOTH_DEVICE_SELECTION (priv->btselector);
			bluetooth_device_selection_start_discovery (selector);
		}
	}
	
	return NULL;
}

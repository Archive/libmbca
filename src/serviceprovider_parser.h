/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file serviceprovider_parser.h
 * @brief database xml parser
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * Defines the interface for Service Provider Database XML parser.
 */
 
#ifndef SERVICEPROVIDER_PARSER_H
#define SERVICEPROVIDER_PARSER_H

#include "mbca_serviceprovider.h"

G_BEGIN_DECLS

/**
 * @brief country
 *
 * contains all the service providers of a country.
 */
struct _ServiceProviderCountry
{
	gchar* code;		/**< ISO 3166-1 alpha-2 code of the country */
	GSList* providers;  /**< singly-linked list of ServiceProvider structs */
};
typedef struct _ServiceProviderCountry ServiceProviderCountry;


/**
 * @brief parse database
 *
 * Parses database file. Database is stored as XML.
 *
 * @note freeing of the database is left for the caller.
 *       use free_service_provider_database () to deallicate the database.
 *
 * @param filename filename of the database file
 * @param database will be set pointing to singly-linked list of
 *                 ServiceProviderCountry structs if call is successful.
 *
 * @returns ' 0' on success
 * @returns '-1' if file could not be opened
 * @returns '-2' if error was encountered while processing the database
 *
 * @note database is trusted to be valid. Check the database always against the
 *       DTD before feeding to this funtion.
 */
gint
parse_service_provider_xml (const gchar* filename, GSList** database);

/**
 * @brief deallocate database
 *
 * Deallocates a previously parset service provider database.
 */
void
free_service_provider_database (GSList** database);

G_END_DECLS

#endif /* SERVICEPROVIDER_PARSER_H */
 

/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file provider_page.c
 * @brief Service Provider page functionality
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 */

#include "callbacks.h"
#include "provider_thread.h" /** @todo remove, this is for thread bug
						*        workaround
						*/

#include <string.h> /* memmove */

void
mbca_provider_page_prepare_cb (GtkAssistant* gtkassistant __attribute__ ((unused)),
					      GtkWidget* page __attribute__ ((unused)),
					      gpointer user_data)
{
	MBCAAssistant* assistant = user_data;
	MBCAAssistantPrivate* priv = assistant->priv;

	/*this is only a workaround of a thread problem..
	 *//** @todo fixme */
	if (!priv->resources_attached)
	{
		g_mutex_lock (priv->trigger_mutex);
		mbca_provider_attach_resources (priv);
		g_mutex_unlock (priv->trigger_mutex);
		priv->resources_attached = TRUE;
	}
}

void
mbca_provider_selection_changed_cb (GtkTreeSelection* selection,
							 gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkTreeModel* provider_model;
	gboolean is_complete;
	GtkTreeIter iter;
	GtkWidget* page;
	MBCAServiceProvider* provider;
	
	if (gtk_tree_selection_count_selected_rows (selection) == 1)
	{
		gtk_tree_selection_get_selected (selection,
								   &provider_model,
								   &iter);
		gtk_tree_model_get (provider_model, &iter,
						PROVIDER_NAME_COLUMN, &priv->provider_name,
						PROVIDER_DATA_COLUMN, &provider,
						-1);
		free_serviceprovider (priv->conf->provider);
		priv->conf->provider = duplicate_serviceprovider (provider);

		is_complete = TRUE;
	}
	else
	{
		is_complete = FALSE;
	}

	page = gtk_assistant_get_nth_page (priv->assistant, PAGE_PROVIDER);
	gtk_assistant_set_page_complete (priv->assistant, page, is_complete);
}

void
mbca_country_selection_changed_cb (GtkTreeSelection* selection,
							gpointer user_data)
{
	MBCAAssistantPrivate* priv = user_data;
	GtkListStore* provider_store;	
	GtkTreeModel* country_model;
	GtkTreeViewColumn* column;
	GtkWidget* provider_list;
	GtkLabel* country_label;
	gchar* countryname;
	gboolean is_sorted;
	GtkTreeIter iter;
	
	if (!gtk_tree_selection_get_selected (selection,
								   &country_model,
								   &iter))
	{
		/* no selection */
		return;
	}
	
	provider_list = GTK_WIDGET (gtk_builder_get_object (priv->builder,
											  "provider_treeview"));
	g_return_if_fail (provider_list);
	
	gtk_tree_model_get (country_model, &iter,
					COUNTRY_NAME_COLUMN, &countryname,
					COUNTRY_PROVIDERS_COLUMN, &provider_store,
					COUNTRY_SORTED_COLUMN, &is_sorted,
					-1);
	gtk_tree_view_set_model (GTK_TREE_VIEW (provider_list),
						GTK_TREE_MODEL (provider_store));

	g_free (priv->country_name);
	priv->country_name = g_strdup (countryname);
	
	country_label = GTK_LABEL ( gtk_builder_get_object (priv->builder,
											  "country_label"));
	g_return_if_fail (country_label);
	countryname = g_strjoin (NULL, _("_Country:"),  " ", countryname, NULL);
	gtk_label_set_text_with_mnemonic (country_label, countryname);
	g_free (countryname);
		
	if (!is_sorted)
	{
		/* sort the list */
		column = gtk_tree_view_get_column (GTK_TREE_VIEW (provider_list),
									PROVIDER_NAME_COLUMN);
		gtk_tree_view_column_set_sort_order (column, GTK_SORT_ASCENDING);
		gtk_tree_view_column_clicked (column);
		
		gtk_list_store_set (GTK_LIST_STORE (country_model), &iter,
						COUNTRY_SORTED_COLUMN, TRUE,
						-1);
	}

}

gboolean
mbca_country_code_qyery_tooltip_cb (GtkWidget* widget,
							 gint x,
							 gint y,
							 gboolean keyboard_mode __attribute__ ((unused)),
							 GtkTooltip* tooltip,
							 gpointer userdata __attribute__ ((unused)))
{
	GtkTreeView* country_list = GTK_TREE_VIEW (widget);
	GtkTreeViewColumn* column;
	GList* columns;
	gint column_id;
	gint length;
	gint bx;
	gint by;
	
	gtk_tree_view_convert_widget_to_bin_window_coords (country_list,
											 x,
											 y,
											 &bx,
											 &by);
	gtk_tree_view_get_path_at_pos (country_list,
							 bx,
							 by,
							 NULL, 
							 &column,
							 NULL,
							 NULL);
	
	gtk_tooltip_set_text (tooltip, _("You can search the list by entering "
							   "text on your keyboard"));
	if (column)
	{
		columns = gtk_tree_view_get_columns (country_list);
	
		length = g_list_length (columns);
		for (column_id = 0; column_id != length ; column_id++)
		{
			if (((GtkTreeViewColumn*)columns->data) == column)
			{
				break;
			}
			columns = columns->next;
		}
		g_list_free (columns);
	
		if (column_id == COUNTRY_CODE_COLUMN)
		{
			gtk_tooltip_set_text (tooltip, _("The country code as defined "
									   "by ISO 3166 standard"));
		}

		gtk_tree_view_set_tooltip_cell (GTK_TREE_VIEW (country_list),
								  tooltip,
								  NULL,
								  column,
								  NULL);	
	}

	return TRUE;	
}

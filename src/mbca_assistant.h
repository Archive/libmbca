/* -*- Mode: C; tab-width: 5; indent-tabs-mode: t; c-basic-offset: 5 -*- */

/* Copyright © 2008 Antti Kaijanmäki <antti@kaijanmaki.net>
 * 
 * This file is part of Mobile Broadband Configuration Assistant.
 *
 * Mobile Broadband Configuration Assistant is free software:
 * you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mobile Broadband Configuration Assistant is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mobile Broadband Configuration Assistant.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file mbca_assistant.h
 * @brief public API
 * @author Antti Kaijanmäki <antti@kaijanmaki.net>
 *
 * Contains API function definitions and datatypes.
 * Forms together with mbca_serviceprovider.h public API of libmbca.
 */
 
#ifndef MBCA_ASSISTANT_H
#define MBCA_ASSISTANT_H

#include "mbca_serviceprovider.h"

#include <glib-object.h>

G_BEGIN_DECLS


/** standard typedef */
typedef struct _MBCAAssistant MBCAAssistant;

/** standard typedef */
typedef struct _MBCAAssistantClass MBCAAssistantClass;

/** standard typedef */
typedef struct _MBCAAssistantPrivate MBCAAssistantPrivate;

/** standard typedef */
typedef struct _MBCAAssistantClassPrivate MBCAAssistantClassPrivate;

/** standard macro */
#define MBCA_TYPE_ASSISTANT (mbca_assistant_get_type ())

/** standard macro */
#define MBCA_ASSISTANT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MBCA_TYPE_ASSISTANT, MBCAAssistant))

/** standard macro */
#define MBCA_ASSISTANT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MBCA_TYPE_ASSISTANT, MBCAAssistantClass))

/** standard macro */
#define MBCA_IS_ASSISTANT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MBCA_TYPE_ASSISTANT))

/** standard macro */
#define MBCA_IS_ASSISTANT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MBCA_TYPE_ASSISTANT))

/** standard macro */
#define MBCA_ASSISTANT_GET_CLASS(obj)(G_TYPE_INSTANCE_GET_CLASS ((obj), MBCA_TYPE_ASSISTANT, MBCAAssistantClass))


/**
 * @brief instance
 *
 * members of an assistant instance.
 */
struct _MBCAAssistant
{
	GObject parent;	/**< derives MBCAAssistant from GObject			  */

	MBCAAssistantPrivate* priv; /**< private bits and pieces			  */
};

/**
 * @brief class
 *
 * members of assistant class.
 *
 * @par signals:
 * <code>
 * void (*state-changed) (MBCAAssistant* assistant,
 *                        enum MBCAAssistantState state,
 *                        gpointer user_data)
 * </code>
 */
struct _MBCAAssistantClass
{
	GObjectClass parent; /**< derives MBCAAssistantClass from GObjectClass  */

	/* signal ids */
	guint state_changed_signal_id; /**< signal ID of "state-changed"		  */

	MBCAAssistantClassPrivate* priv; /**< private bits and pieces		  */
};

/**
 * @brief standard function
 *
 * GObject boilerplate code
 */
GType mbca_assistant_get_type (void);


/**
 * @brief type of device
 *
 * Type of the device (and connection).
 */
typedef enum   /*< skip >*/
{
	MBCA_DEVICE_NONE,	   /**< no device selected, for initialization	  */
	MBCA_DEVICE_BLUETOOTH, /**< Bluetooth device						  */
	MBCA_DEVICE_HAL,	   /**< devices discovered using HAL			  */
	MBCA_DEVICE_SERIAL,	   /**< Serial device						  */
	MBCA_DEVICE_PSEUDO	   /**< pseudo device, not shown on summary page	  */
} MBCADeviceType;

/**
 * @brief connection information
 *
 * Contains all the information needed for establishing a connection.
 */
struct _MBCAConfiguration
{
	gchar* name;			/**< name of the connection			       */
	MBCADeviceType type;	/**< type of the connection				  */
	gchar* device;			/**< device to use, format depends on type
						 *   @see mbca_assistant_run_for_device()	  */
	gchar* baud;			/**< baud rate to use with the device		  */
	
	MBCAServiceProvider* provider; /**< service provider specific settings	*/
};
/** standard typedef */
typedef struct _MBCAConfiguration MBCAConfiguration;


/**
 * @brief free configuration
 * 
 * deallocates previously allocated MBCAConfiguration.
 *
 * @note this function should be used only with configurations that are build
 *       in mbca_assistant_run() or mbca_assistant_run_for_device().
 *
 * @param configuration configuration to be freed
 */
void
mbca_free_configuration (MBCAConfiguration* configuration);


/**
 * @brief states of MBCAAssistant
 */
typedef enum /*< prefix=MBCA,underscore_name=mbca_assistant_state >*/
{
	MBCA_STATE_READY,    /**< Assistant is ready to be run */
	MBCA_STATE_RUNNING,  /**< Assistant is running	     */
	MBCA_STATE_ABORTED,  /**< Assistant was aborted        */
	MBCA_STATE_DONE	 /**< Assistant has finished       */
} MBCAAssistantState;

/**
 * @brief create new instance
 *
 * Creates new instance of MBCAAssistant. The instance must be unreferenced
 * when not anymore needed with g_object_unref()
 *
 * @note gtk_init() and g_thread_init() must be called before this function
 *
 * @returns a newly allocated instance.
 */
MBCAAssistant*
mbca_assistant_new ();


/**
 * @brief run fully
 *
 * Runs the assistant fully. The assistant contains 5 pages:
 *    1. introduction
 *    2. connection method
 *    3. device page based according to method
 *    4. service provider
 *    5. summary
 *
 * actually calls:
 * <code>
 *        mbca_assistant_run_for_device (assistant,
 *                                       MBCA_DEVICE_NONE,
 *                                       NULL, NULL);
 * </code>
 *
 * @see mbca_assistant_run_for_device()
 *
 * @note assistant must be in MBCA_STATE_READY state
 *
 * @param assistant assistant to run
 *
 * @returns ' 0' on success
 * @returns '-1' on failure
 */
gint
mbca_assistant_run (MBCAAssistant* assistant);

/**
 * @brief run partially
 *
 * Runs the assistant. If device type is MBCA_DEVICE_NONE assistant is
 * run fully. Otherwise assistant skips device selection and service provider
 * page follows directly after introduction page. If device type is
 * MBCA_DEVICE_PSEUDO device field on summary page is also hidden.
 *
 * After assistant is once run it can't be run again. Unref the old and create
 * a new one if necessary.
 *
 *
 * format for device depends from type:
 *
 * MBCA_DEVICE_BLUETOOTH
 *  - device is Bluetooth device address
 *  - e.g. "01:23:45:67:89:AB"
 *
 * MBCA_DEVICE_HAL
 *  - device is HAL UDI of a device that belongs in info.catecory "serial"
 *  - e.g. "/org/freedesktop/Hal/devices/usb_device_421_453_noserial_if0_3_
 *          serial_unknown_0"
 *
 * MBCA_DEVICE_SERIAL
 * - device is path to character device representing serial port. For serial
 *   devices nice_device_name means baud rate!
 * - e.g. "/dev/ttyS0", "9600"
 *
 * MBCA_DEVICE_PSEUDO
 * - device and nice_device_name are ignored
 *
 * @see mbca_assistant_run()
 *
 * @note assistant must be in MBCA_STATE_READY state
 *
 * @param assistant assistant to run
 * @param type device type to preset
 * @param device device to preset
 * @param nice_device_name name for the device that is displayd to the user
 *
 * @returns ' 0' on success
 * @returns '-1' on failure
 */
gint
mbca_assistant_run_for_device (MBCAAssistant* assistant,
							MBCADeviceType type,
						 const gchar* device,
						 const gchar* nice_device_name);

/**
 * @brief get state
 *
 * @param assistant assistant to get state from
 *
 * @returns assistants state
 */			 
MBCAAssistantState
mbca_assistant_get_state (MBCAAssistant* assistant);

/**
 * @brief get configuration
 *
 * @note assistant must be in state MBCA_STATE_DONE
 * @note callee is responciple of calling mbca_free_configuration() when
 *       returned configuration is no longer needed.
 * 
 * @param assistant assistant which configuration to retreive
 *
 * @returns configuration of user selection
 */
MBCAConfiguration*
mbca_assistant_get_configuration (MBCAAssistant* assistant);

/**
 * @brief abort assistant
 * 
 * @param assistant assistant to abort
 *
 * aborts assistant if in RUNNING state and sets state to MBCA_STATE_ABORTED
 */
void
mbca_assistant_abort (MBCAAssistant* assistant);

/**
 * @brief presents assistant to user
 *
 * Presents an already running assistant to the user.
 *
 * @param assistant assistant to present
 */
void
mbca_assistant_present (MBCAAssistant* assistant);

G_END_DECLS

#endif /* MBCA_ASSISTANT_H */
